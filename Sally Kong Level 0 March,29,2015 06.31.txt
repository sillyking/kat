Words/min: 29.72339
Characters/min: 77.28081
Time to finish this level: 0min 20sec
Total mistakes: 2
Total words: 10
Total words typed: 10
Total characters typed: 26
Total time played: 0min 20sec
===========================================
Target Word	User's Input
 na		a
 are		re
 ear		ar
 ae		re
 ae		ne
 ae		e
 near		ear
 er		r
 arn		rn
 are		re
