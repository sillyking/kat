using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Finger : MonoBehaviour {

	public string quadrant;
	public Vector3 ref_position;
	public Vector3 last_position;
	public Vector3 init_position;

	float timer;

	public float hold;
	public float horizontalThreshold;
	public float verticalThreshold;

	public string[] gestures;
	public int gestureTransition;
	public bool release;
	bool vibrate;

	public Finger(Vector3 pos, float hDist, float vDist, float vibHold, bool vib) {
		quadrant = "";
		init_position = pos;
		ref_position = pos;
		last_position = pos;
		timer = 0;
		gestures = new string[2];
		gestureTransition = 0;
		release = false;

		horizontalThreshold = hDist;
		verticalThreshold = vDist;
		hold = vibHold;
		vibrate = vib;
	}
	
	void Start() {
		timer = 0;
		hold = 0.3f;
		horizontalThreshold = 2.0f;
		verticalThreshold = 5.5f;
		gestures = new string[2];
		gestureTransition = 0;
		release = false;
		vibrate = false;
	}

	public void changeParam(float hDist, float vDist, float vibHold, bool vib) {
		horizontalThreshold = hDist;
		verticalThreshold = vDist;
		hold = vibHold;
		vibrate = vib;
	}

	public void updatePos(Vector3 pos) {
		last_position = pos;
	}

	public void die() {
		if(gameObject != null){
			Destroy (gameObject);
		}
	}

	public void Update() {

		if(quadrant != null && string.Compare(quadrant, "") != 0) {
			timer += Time.deltaTime;
		}

		if (gestureTransition <= 2) {
			computeGesture ();
		}
	}

	public void setInit(Vector3 pos) {
		init_position = pos;
	}

	public void updateLast(Vector3 pos) {
		last_position = pos;
	}

	public void addGesture(string gest) {
		if(gestureTransition == 0 || gestureTransition == 1) {
			gestures[gestureTransition] = gest;
			gestureTransition++;
		}
	}
	
	void computeGesture() {

		//for hold
		if ( Vector3.Distance(ref_position, last_position) <= 1.0f 
		    	&& timer >= hold
			    && string.Compare(quadrant, "") != 0 
		    	&& quadrant != null && !release) {
		
			if(gestureTransition == 1 && gestures[0] == "|") {

			} else {
				timer = 0;
				if(gestureTransition < 2) {
					gestures[gestureTransition] = "|";
					gestureTransition++;
					if(vibrate) {
						//comment out for server
						//Vibration.Vibrate(100);
					}
				}
			}

			//for forward
		} else if (ref_position.x - last_position.x >= horizontalThreshold) {
			
			// can't have front and front again
			if((gestureTransition == 1 && gestures[0] == "<") 
			   || (gestureTransition == 2 && gestures[1] == "<")
			   || (gestureTransition == 1 && gestures[0] == "^")
			   || (gestureTransition == 1 && gestures[0] == "v")){	
				ref_position = last_position;
				
				// if the user is just moving his finger left to right
			} else if(gestureTransition == 1 && gestures[0] == ">"){
				gestures[0] = "<";
				timer = 0;
				
				// if the user is just moving his finger left to right
			} else if (gestureTransition == 2 && gestures[1] == ">") {
				gestures[1] = "<";
				timer = 0;
				
				//valid input for <
			} else {
				if(gestureTransition < 2){
					gestures[gestureTransition] = "<";
					gestureTransition++;
				}
				timer = 0;
				ref_position = last_position;
			}
			
			//for backward
		} else if (last_position.x - ref_position.x >= horizontalThreshold){
			
			// can't have back and back again
			if((gestureTransition == 1 && gestures[0] == ">") 
			   || (gestureTransition == 2 && gestures[1] == ">")
			   || (gestureTransition == 1 && gestures[0] == "^")
			   || (gestureTransition == 1 && gestures[0] == "v")){	
				
				ref_position = last_position;
				
				// if the user is just moving his finger left to right
			} else if (gestureTransition == 1 && gestures[0] == "<") {
				gestures[0] = ">";
				timer = 0;
				
				// if the user is just moving his finger left to right
			} else if (gestureTransition == 2 && gestures[1] == "<") {
				gestures[1] = ">";
				timer = 0;
				
				//valid input for >
			} else {
				if(gestureTransition < 2) {
					gestures[gestureTransition] = ">";
					gestureTransition++;
				}
				timer = 0;
				ref_position = last_position;
			}
		
		//for vertical swipe down
		} else if(init_position.y > last_position.y 
		          && Mathf.Abs(init_position.y - last_position.y) >= verticalThreshold) {
			// can't be a second gesture
			if(gestureTransition > 0) {	
				ref_position = last_position;
			} else {
				gestures[gestureTransition] = "v";
				gestureTransition++;
				
				timer = 0;
				ref_position = last_position;
			}
			
			
		//for vertical swipe up
		} else if(init_position.y < last_position.y 
		          && Mathf.Abs(init_position.y - last_position.y) >= verticalThreshold) {
			// can't be a second gesture
			if(gestureTransition > 0){	
				ref_position = last_position;
			} else {
				gestures[gestureTransition] = "^";
				gestureTransition++;
				
				timer = 0;
				ref_position = last_position;
			}
		}	
	}
}

