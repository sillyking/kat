﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GlobalScript : MonoBehaviour {
	
	public Dictionary<int, GameObject> trails;
	
	public string state;
	public string output;
	
	Dictionary<int, Finger> fingers;
	float fingerCnt;
	int leftCnt;
	int rightCnt;
	
	float hDist;
	float vDist;
	float vibHold;
	bool vibrate;
	
	Queue<Finger> reservationQueue;
	float quadX, quadY;
	float sixY;
	bool l3, r3;
	
	string l1;
	string l2;
	string r1;
	string r2;
	
	public GUIStyle KB;
	public GUIStyle buttonStyle;
	public GUIStyle swipesLabel;
	public GUIStyle gestures;
	public GUIStyle vertGestures;
	public GUIStyle config;

	Color deadKB;
	Color defaultKB;
	Color semiHighlight;
	Color highlight;
	
	int keyCount;	//7 keys
	int[][] activeFingers;
	string[] keys;
	
	string mode;	//alpha, CAPSLock, numLock, specialLock
	public int setting;	//0 for standAlone 1 for desktop Connection
	
	ClientControl user;
	Control socket;

	bool fineTuning;
	bool trailsToggle;
	bool displayOn; //highlights
	bool sixFingers;
	
	string message;

	//for game
	public bool playGame;
	public int score; 
	float gameTimer;

	void Awake() {
		Application.targetFrameRate = 20;
	}
	
	// Use this for initialization
	void Start () {
		
		keyCount = 7;
		fingerCnt = 0;
		leftCnt = 0;
		rightCnt = 0;

		state = "";
		output = "";
		setting = 1;
		message = "";
		
		trails = new Dictionary<int, GameObject> ();
		fingers = new Dictionary<int, Finger> ();
		reservationQueue = new Queue<Finger> ();

		deadKB = Color.Lerp (Color.black, Color.gray, 0.5f);
		defaultKB = Color.Lerp (Color.white, Color.gray, 0.6f);
		semiHighlight = Color.Lerp (Color.red, Color.yellow, 0.7f);
		highlight = Color.yellow;

		quadX = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width/2, 0, 0)).x;
		quadY = Camera.main.ScreenToWorldPoint(new Vector3(0, Screen.height/2, 0)).y;
		sixY = Camera.main.ScreenToWorldPoint(new Vector3(0, Screen.height/4, 0)).y;
		
		mode = "alpha";
		l1 = "lsue,cq";
		l2 = "digrxkv";
		r1 = "wb.aotm";
		r2 = "jyznfhp";

		//toggle for 6 finger mode
		l3 = false;
		r3 = false;

		keys = new string[4] {r1, r2, l1, l2};
		activeFingers = new int[4][] {new int[keyCount], new int[keyCount], 
			new int[keyCount], new int[keyCount]};

		modeUpdate ();

		hDist = 2.0f;
		vDist = 1.5f;
		vibHold = 0.5f;
		vibrate = true;
		
		GameObject g = GameObject.Find ("Sockets");
		user = g.GetComponent<ClientControl> ();
		socket = g.GetComponent<Control> ();

		trailsToggle = true;
		displayOn = true;
		sixFingers = false;

		playGame = false;
		score = 0;

	}

	public void modeUpdate() {

		string l1New = l1;
		string l2New = l2;
		string r1New = r1;
		string r2New = r2;

		switch(mode) {
		case "alpha":
			if(setting == 1) {
				r1New = l1;
				r2New = l2;
				l1New = r1;
				l2New = r2;
			} else if(setting == 0){
				l1New = l1;
				l2New = l2;
				r1New = r1;
				r2New = r2;
			}
			break;
		case "CAPSLock":
			if(setting == 1) {
				r1New = capitalize (l1);
				r2New = capitalize (l2);
				l1New = capitalize (r1);
				l2New = capitalize (r2);
			} else if(setting == 0){
				l1New = capitalize (l1);
				l2New = capitalize (l2);
				r1New = capitalize (r1);
				r2New = capitalize (r2);
			}
			break;
		case "numLock":
			if(setting == 1) {
				r1New = "12345+-";
				r2New = "67890*/";
				l1New = "=|%()[]";
				l2New = "!#<>^:;";
			} else if(setting == 0){
				l1New = "12345+-";
				l2New = "67890*/";
				r1New = "=|%()[]";
				r2New = "!#<>^:;";
			}
			break;
		case "specialLock":
			if(setting == 1) {
				r1New = ".,?!@#$";
				l1New = "~*|()-_";
				l2New = "+=%[]^&";
				r2New = "\"'`{}\\/";
			} else if(setting == 0) {
				l1New = ".,?!@#$";
				r1New = "~*|()-_";
				r2New = "+=%[]^&";
				l2New = "\"'`{}\\/";
			}
			break;
		}
		
		keys[0] = stringReverse(l1New);
		keys[1] = stringReverse(l2New);
		keys[2] = stringReverse(r1New);
		keys[3] = stringReverse(r2New);
	}
	
	void Update() {

		
		if((socket.gotIP || setting == 0) && !fineTuning) {
			
			if (Input.touchCount == 0 && reservationQueue.Count != 0) {
				fingerCnt = 0;
				leftCnt = 0;
				rightCnt = 0;
			} else {
				fingerCnt = Input.touchCount;
			}
			
			if(setting == 1) {
				message = "";
			}
			
			//Check for all fingers
			for (int i = 0; i < Input.touchCount; i++) {

				Touch touch = Input.GetTouch(i);
				int currentID = touch.fingerId;

				//if fingers are at the menu bar break
				if(touch.position.y >= Screen.height - 50 && touch.position.y <= Screen.height - 10) {
					break;
				}
				
				Vector3 position = Camera.main.ScreenToWorldPoint(touch.position);
				position.z = 0; // Make sure the trail is visible
				
				if (touch.phase == TouchPhase.Began) {

					if(fingers.ContainsKey(currentID) == false) {

						Finger fing = new Finger(position, hDist, vDist, vibHold, vibrate);
					
						fing.init_position = position;
						fing.ref_position = position;
						setQuad(fing);

						if(sixFingers) {
							//in 6 finger mode, make the finger invalid if l3 and r3 aren't set
							if(fing.quadrant == "L3" || fing.quadrant == "R3" || (l3 && r3)) {
								fing.verticalThreshold = Screen.height;
							} else {
								if(fing.quadrant == "L1" || fing.quadrant == "L2") {
									leftCnt--;
									//if both l3 and r3 aren't in contact, horizontal swipe
									if(!l3 && !r3) {
										fing.quadrant = "L4";
									} else {
										fing.quadrant = "";
									}
								} else if (fing.quadrant == "R1" || fing.quadrant == "R2") {
									//if both l3 and r3 aren't in contact, horizontal swipe
									rightCnt--;
									if(!l3 && !r3) {
										fing.quadrant = "R4";
									} else {
										fing.quadrant = "";
									}
								}
							}
						}

						if(fing.quadrant != "") {
							//valid finger
							fingers.Add(currentID, fing);
							if(fing.quadrant != "L3" && fing.quadrant != "R3") {
								reservationQueue.Enqueue(fing);
								fing.Update();

								//make trail
								if (!trails.ContainsKey(currentID) && trailsToggle) {
									if(setting == 1) {
										message += (char) 25;
										message += currentID;
										message += position.x;
										message += (char) 26;
										message += position.y;
										message += (char) 26;
									}
									GameObject trail = SpecialEffects.MakefTrail(position);
									trails.Add (currentID, trail);
								}
							}
						} else {
							Destroy(fing);
						}
					}
					
				} else if (touch.phase == TouchPhase.Moved) {
					
					//move trail
					if (trails.ContainsKey(currentID) && trailsToggle) {
						GameObject trail = trails[currentID];							
						trail.transform.position = position;
						if(setting == 1) {
							message += (char) 25;
							message += currentID;
							message += position.x;
							message += (char) 26;
							message += position.y;
							message += (char) 26;
						}
					}
					
					//if finger gesture combo is already complete, output the text
					if(fingers.ContainsKey(currentID)){
						fingers[currentID].updateLast(position);
					}
					
				} else if(touch.phase == TouchPhase.Stationary) {
					if(fingers.ContainsKey(currentID)){
						fingers[currentID].ref_position = position;
						fingers[currentID].updateLast(position);
					} 
				} else if (touch.phase == TouchPhase.Ended) {
					
					// Clear known trails
					if (trails.ContainsKey(currentID) && trailsToggle) {
						GameObject trail = trails[currentID];
						if(setting == 1) {
							message += (char) 25;
							message += currentID;
							message += (char) 27;
						}
						// Let the trail fade out
						Destroy (trail, trail.GetComponent<TrailRenderer>().time);
						trails.Remove(currentID);
					}
					
					if(fingers.ContainsKey(currentID)) {

						if(sixFingers){
							if(fingers[currentID].quadrant == "L3") {
								l3 = false;
								if(setting == 1){
									message += (char)30;
									message += "0";
								}
								Finger temp = fingers[currentID];
								Destroy(temp);
							} else if(fingers[currentID].quadrant == "R3") {
								r3 = false;
								if(setting == 1) {
									message += (char)31;
									message += "0";
								}
								Finger temp = fingers[currentID];
								Destroy(temp);
							}
						}
						
						if(fingers[currentID].gestures[0] == null) {
							fingers[currentID].addGesture("|");
						}

						if(fingers[currentID].quadrant == "R1" || 
						   fingers[currentID].quadrant == "R2") {
							rightCnt--;
						} else if (fingers[currentID].quadrant == "L1" || 
						           fingers[currentID].quadrant == "L2") {
							leftCnt--;
						}
						
						// >- or <- or |-
						if(fingers[currentID].gestures[0] != null &&
						   fingers[currentID].gestures[1] == null) {
							fingers[currentID].addGesture("-");
							fingers[currentID].release = true;
							fingers.Remove(currentID);
						} else if(fingers[currentID].gestureTransition == 2) {
							fingers[currentID].release = true;
							fingers.Remove(currentID);
						} else {
							fingers.Remove(currentID);
						}
					}
					
				}
				
				if(fingers.Count > 0) {
					foreach(Finger f in fingers.Values) {
						f.Update();
					}
				}
			}
			
			outputUpdate ();
			if(setting == 1) {
				user.input = message;
				user.send = true;
			}
		}


		if(playGame) {
			//game is over
			if(gameTimer >= 30.0f) {
				playGame = false;
				reset ();
			}
			gameTimer += Time.deltaTime;
		}
	}
	
	void setQuad(Finger fing) {
		//for left hand
		if(fing.init_position.x >= quadX && leftCnt < 2) {

		//Check for lowest if 6 finger mode		
		if(sixFingers && fing.init_position.y <= sixY && !l3) {
			l3 = true;
			if(setting == 1) {
				message += (char) 30;
				message += "1";
			}
			fing.quadrant = "L3";
		} else {
			leftCnt++;
			if(fing.init_position.y >= quadY){

				fing.quadrant = "L1";

				//Check if L1 already exists
				if(fingers.Count > 0) {
					foreach (int i in fingers.Keys) {
						if(fingers[i].quadrant == "L1") {
							if(fingers[i].init_position.y > fing.init_position.y) {
								//calibrate quadrant's Y-divider
								quadY = (fingers[i].init_position.y + fing.init_position.y)/2.0f;
								fing.quadrant = "L2";
							} else {
								quadY = (fingers[i].init_position.y + fing.init_position.y)/2.0f;
								fingers[i].quadrant = "L2";
							}
						}
					}
				}
			} else {
				fing.quadrant = "L2";
				//Check if L2 already exists
				if(fingers.Count > 0) {
					foreach (int i in fingers.Keys) {
						if(fingers[i].quadrant == "L2") {
							if(fingers[i].init_position.y < fing.init_position.y) {
								//calibrate quadrant's Y-divider
								quadY = (fingers[i].init_position.y + fing.init_position.y)/2.0f;
								fing.quadrant = "L1";
							} else {
								//calibrate quadrant's Y-divider
								quadY = (fingers[i].init_position.y + fing.init_position.y)/2.0f;
								fingers[i].quadrant = "L1";
							}
						}
					}
				}
			}
		 }
		} else if(fing.init_position.x <= quadX && rightCnt < 2) {

			//for right hand

			if(sixFingers && fing.init_position.y <= sixY && !r3) {
				r3 = true;
				if(setting == 1) {
					message += (char) 31;
					message += "1";
				}
				fing.quadrant = "R3";
			} else {
			 rightCnt++;
			 if(fing.init_position.y >= quadY){
				fing.quadrant = "R1";

				//Check if R1 already exists
				if(fingers.Count > 0) {
					foreach (int i in fingers.Keys) {
						if(fingers[i].quadrant == "R1") {
							if(fingers[i].init_position.y > fing.init_position.y) {
								fing.quadrant = "R2";
								//calibrate quadrant's Y-divider
								quadY = (fingers[i].init_position.y + fing.init_position.y)/2.0f;
							} else {
								fingers[i].quadrant = "R2";
								//calibrate quadrant's Y-divider
								quadY = (fingers[i].init_position.y + fing.init_position.y)/2.0f;
							}
						}
					}
				}
			} else {
				fing.quadrant = "R2";
				//Check if R2 already exists
				if(fingers.Count > 0) {
					foreach (int i in fingers.Keys) {
						if(fingers[i].quadrant == "R2") {
							if(fingers[i].init_position.y < fing.init_position.y) {
								fing.quadrant = "R1";
								//calibrate quadrant's Y-divider
								quadY = (fingers[i].init_position.y + fing.init_position.y)/2.0f;
							} else {
								fingers[i].quadrant = "R1";
								//calibrate quadrant's Y-divider
								quadY = (fingers[i].init_position.y + fing.init_position.y)/2.0f;
							}
						}
					}
				}
			}
		 }
		} else {
			fing.quadrant = "";
		}
	}
	
	void outputUpdate() {
		
		if(reservationQueue.Count > 0) {
			
			Finger front = reservationQueue.Peek();
			
			if(front.release && front.gestureTransition == 2) {

				getMappedKey(front);

				reservationQueue.Dequeue();
				Destroy(front);
				
				
			} 	else if(fingerCnt == 0) {
				// if it's in front and not complete, 
				//and the finger is off it's an invalid finger
				reservationQueue.Dequeue();
				Destroy (front);
				fingers.Clear();
			}
		}
		
		//GUI Keyboard
		refreshKeyBoard ();
		
		string debugString = "In the reserveQueue" + '\n';
		
		Finger[] checking = reservationQueue.ToArray ();
		for(int i = 0; i < checking.Length; i++) {
			
			string gestureStuff = checking[i].gestureTransition + " transitions";
			int ind = checking[i].gestureTransition;
			if(ind == 1) {
				gestureStuff += " " + checking[i].gestures[ind - 1];
			} else if(ind == 2) {
				gestureStuff += " ("  + checking[i].gestures[ind - 2] + "," + checking[i].gestures[ind - 1] + ") combo";
			}
			
			debugString += "Finger #" + checking[i].quadrant + 
				"Released: " + checking[i].release + '\n' +
					"InitPos: " + checking[i].init_position + " Pos: " + checking[i].last_position + '\n';
			debugString += gestureStuff + '\n';	
			
			//for the GUI
			if(!checking[i].release) {
				updateActive (checking[i]);
			}		
		}
		state = debugString;
		state += "Six fingers? " + sixFingers + '\n' + 
			l3 + "," + r3 + '\n' +
				"left count: " + leftCnt + ", right cnt: " + rightCnt;
	}
	
	void getMappedKey(Finger fing) {
		
		//Printing Mapped Characters
		int currentFinger = 6;
		
		switch(fing.quadrant) {
		case "R1":
			currentFinger = 0;
			break;
		case "R2":
			currentFinger = 1;
			break;
		case "L1":
			currentFinger = 2;
			break;
		case "L2":
			currentFinger = 3;
			break;	
		//for 6 finger mode
		case "R4":
			currentFinger = 4;
			break;
		case "L4":
			currentFinger = 5;
			break;
		}
		
		if(setting == 1) {
			//message: eventFlag + currentFinger + finger Gestures
			message += (char) 21;
			message += currentFinger.ToString();
			message += fing.gestures[0];
			message += fing.gestures[1];
		}

		if(currentFinger < 4) {
			switch (fing.gestures[0]) {
			case "|":
				switch (fing.gestures[1]) {
				case "-":
					output += keys[currentFinger].Substring(3, 1);
					break;
				case ">":
					output += keys[currentFinger].Substring(4, 1);
					break;
				case "<": 
					output += keys[currentFinger].Substring(2, 1);
					break;
				}
				modeUpdate();
				break;
			case ">":
				switch (fing.gestures[1]) {
				case "-":
					output += keys[currentFinger].Substring(5, 1);
					break;
				case "|": 
					output += keys[currentFinger].Substring(6, 1);
					break;
				} 
				modeUpdate();
				break;
			case "<":
				switch (fing.gestures[1]) {
				case "-":
					output += keys[currentFinger].Substring(1, 1);
					break;
				case "|": 
					output += keys[currentFinger].Substring(0, 1);
					break;
				} 
				modeUpdate();
				break;
			case "^":
				switch (fing.gestures[1]) {
				case "-": 
					//right hand Up Swipe
					if(currentFinger == 0 || currentFinger ==1) {
						if(output.Length >= 1 ) {
							output = output.Substring(0, output.Length-1);
						}
					} else {
					//left hand Up Swipe
						if(mode != "numLock") {
							mode = "numLock";
							modeUpdate();
						} else {
							mode = "alpha";
							modeUpdate();
						}
					}
					modeUpdate();
					break;
				case "|":
					//right hand Up Hold
					if(currentFinger == 0 || currentFinger ==1) {
						//clear
						reset();
					} else {
						//left hand Up Hold
						if(mode != "specialLock") {
							mode = "specialLock";
							modeUpdate();
						} else {
							mode = "alpha";
							modeUpdate();
						}
					}
					break;
				}
				break;
			case "v":
				switch (fing.gestures[1]) {
				case "-": 
					//right hand Down Swipe
					if(currentFinger == 0 || currentFinger ==1) {
						output += " ";
					} else {
						//left hand Down Swipe
						if(mode != "CAPSLock") {
							if(setting == 0) {
								keys[0] = stringReverse(capitalize (l1));
								keys[1] = stringReverse(capitalize (l2));
								keys[2] = stringReverse(capitalize (r1));
								keys[3] = stringReverse(capitalize (r2));
							} else if(setting == 1) {
								keys[2] = stringReverse(capitalize (l1));
								keys[3] = stringReverse(capitalize (l2));
								keys[0] = stringReverse(capitalize (r1));
								keys[1] = stringReverse(capitalize (r2));
							}
						} else {
							if(setting == 0) {
								keys[0] = stringReverse(l1);
								keys[1] = stringReverse(l2);
								keys[2] = stringReverse(r1);
								keys[3] = stringReverse(r2);
							} else if(setting == 1) {
								keys[2] = stringReverse(l1);
								keys[3] = stringReverse(l2);
								keys[0] = stringReverse(r1);
								keys[1] = stringReverse(r2);
							}
						}
					}
					break;
				case "|":
					//right hand Down Hold
					if(currentFinger == 0 || currentFinger ==1) {
						output += char.ToString('\n');
					} else {
						//left hand Down Hold
						if(mode != "CAPSLock") {
							mode = "CAPSLock";
							modeUpdate();
						} else {
							mode = "alpha";
							modeUpdate();
						}
					}
					break;
				}
				break;
			}
		} else if(currentFinger < 6) {
			switch (fing.gestures[0]) {
			case "<":
				switch (fing.gestures[1]) {
				case "-": 
					//right hand Up Swipe
					if(currentFinger == 4) {
						if(setting == 1) {
							output += " ";
						} else {
							output = output.Substring(0, output.Length-1);
						}
					}
					modeUpdate();
					break;
				case "|":
					//right hand Up Hold
					if(currentFinger == 4) {
						if(mode != "numLock") {
							mode = "numLock";
							modeUpdate();
						} else {
							mode = "alpha";
							modeUpdate();
						}
					} else {
						//left hand Up Hold
						if(mode != "specialLock") {
							mode = "specialLock";
							modeUpdate();
						} else {
							mode = "alpha";
							modeUpdate();
						}
					}
					break;
				}
				break;
			case ">":
				switch (fing.gestures[1]) {
				case "-": 
					//right hand Down Swipe
					if(currentFinger == 4) {
						if(setting == 1) {
							output = output.Substring(0, output.Length-1);
						} else {
							output += " ";
						}
					} else {
						//left hand Down Swipe
						if(mode != "CAPSLock") {
							if(setting == 1) {
								keys[2] = stringReverse(capitalize (l1));
								keys[3] = stringReverse(capitalize (l2));
								keys[0] = stringReverse(capitalize (r1));
								keys[1] = stringReverse(capitalize (r2));
							} else if(setting == 0) {
								keys[0] = stringReverse(capitalize (l1));
								keys[1] = stringReverse(capitalize (l2));
								keys[2] = stringReverse(capitalize (r1));
								keys[3] = stringReverse(capitalize (r2));
							}
						} else {
							if(setting == 1) {
								keys[2] = stringReverse(l1);
								keys[3] = stringReverse(l2);
								keys[0] = stringReverse(r1);
								keys[1] = stringReverse(r2);
							} else if(setting == 0){
								keys[0] = stringReverse(l1);
								keys[1] = stringReverse(l2);
								keys[2] = stringReverse(r1);
								keys[3] = stringReverse(r2);
							}
		
						}
					}
					break;
				case "|":
					//right hand Down Hold
					if(currentFinger == 4) {
						output += char.ToString('\n');
					} else {
						//left hand Down Hold
						if(mode != "CAPSLock") {
							mode = "CAPSLock";
							modeUpdate();
						} else {
							mode = "alpha";
							modeUpdate();
						}
					}
					break;
				}
				break;
			}
		}
	}
	
	void updateActive(Finger fing) {
		
		int currentFinger = 4;
		if(!fing.release) {
			switch(fing.quadrant) {
			case "R1":
				currentFinger = 0;
				break;
			case "R2":
				currentFinger = 1;
				break;
			case "L1":
				currentFinger = 2;
				break;
			case "L2":
				currentFinger = 3;
				break;
				
			}
			//send message here should include currentFinger, gestures 
			if(setting == 1 && displayOn) {
				//messag: eventFlag + currentFinger + finger Gestures
				if(fing.gestureTransition > 0) {

					message += (char) 22;
					message += currentFinger.ToString();
					message += fing.gestures[0];
					if(fing.gestureTransition == 2) {
						message += fing.gestures[1];
					} else {
						message += "_";
					}

				}
			}
		}
		
		if(currentFinger < 4) {
			if(fing.gestureTransition == 1) {
				switch(fing.gestures[0]) {
				case "|":
					activeFingers[currentFinger][4] = 2;
					activeFingers[currentFinger][3] = 1;
					activeFingers[currentFinger][2] = 2;
					break;
				case ">":
					activeFingers[currentFinger][5] = 1;
					activeFingers[currentFinger][6] = 2;
					break;
				case "<":
					activeFingers[currentFinger][0] = 2;
					activeFingers[currentFinger][1] = 1;
					break;
				case "^":
					break;
				case "v":
					break;
				}
			} else if(fing.gestureTransition == 2) {
				switch (fing.gestures[0]) {
				case "|":
					switch (fing.gestures[1]) {
					case "-":
						activeFingers[currentFinger][3] = 1;
						break;
					case ">":
						activeFingers[currentFinger][4] = 1;
						break;
					case "<": 
						activeFingers[currentFinger][2] = 1;
						break;
					case "^":
						break;
					case "v":
						break;
					}
					break;
				case ">":
					switch (fing.gestures[1]) {
					case "-":
						activeFingers[currentFinger][5] = 1;
						break;
					case "|": 
						activeFingers[currentFinger][6] = 1;
						break;
					} 
					break;
				case "<":
					switch (fing.gestures[1]) {
					case "-":
						activeFingers[currentFinger][1] = 1;
						break;
					case "|": 
						activeFingers[currentFinger][0] = 1;
						break;
					} 
					break;
				case "^":
					break;
				case "v":
					break;
				}
			}
		}
	}
	
	void refreshKeyBoard() {
		activeFingers = new int[4][] {new int[keyCount], new int[keyCount], 
			new int[keyCount], new int[keyCount]};
		if(setting == 1) {
			//message += "k";
			message += (char) 23;
		}
	}
	
	void reset() {
		keyCount = 7;
		fingerCnt = 0;
		l3 = false;
		r3 = false;

		state = "";
		output = "";
		
		if(setting == 1) {
			message = "";
			message += (char) 24;
			user.input = message;
			user.send = true;
		}
		
		fingers.Clear ();
		reservationQueue.Clear ();
		trails.Clear ();
		
		refreshKeyBoard();
		mode = "alpha";
		modeUpdate();

		quadX = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width/2, 0, 0)).x;
		quadY = Camera.main.ScreenToWorldPoint(new Vector3(0, Screen.height/2, 0)).y;
		sixY = Camera.main.ScreenToWorldPoint(new Vector3(0, Screen.height/4, 0)).y;

	}

	void startTypeGame() {
		score = 0;
	//	typeGame.reset();
	//	typeGame.play = true;
		reset ();
		playGame = true;
	}
	
	void OnGUI() {
		
		if(socket.gotIP){
			if(GUI.Button (new Rect(10, 10, 145, 40), 
			               "Configuration", buttonStyle)) {
				fineTuning = !fineTuning;
			}

			if(GUI.Button(new Rect(Screen.width - 100, 10, 80, 40), 
			              "Exit", buttonStyle)) {
				Application.Quit ();
				if(setting == 1) {
					user.input = "" + (char)28;
					user.send = true;
				}
			}

			if(!playGame) {
				if(setting == 1 && socket.gotIP) {
					if(GUI.Button(new Rect(Screen.width/2 - 90, 10, 80, 40), "Reset", buttonStyle)) {
						reset();
					} 
					if(GUI.Button (new Rect(Screen.width/2 + 10, 10, 145, 40), "Tutorial Game", buttonStyle)) {
							message += (char) 20;
							message += "1";
							user.input = message;
							user.send = true;
						//	startTypeGame ();
						//	playGame = true;
					}
				} else {
					if(GUI.Button(new Rect(Screen.width/2 - 40, 10, 80, 40), 
					              "Reset", buttonStyle)) {
						reset();
					} 
				}
				//gestures
				GUI.Label ( new Rect(Screen.width/2 - 395, Screen.height-135, 283, 30), "", gestures);
				GUI.Label ( new Rect(Screen.width/2 + 107, Screen.height-135, 283, 30), "", gestures);
			} else {
				if(PlayerPrefs.HasKey("BestScore")) {
						GUI.Label (new Rect(Screen.width/2 - 100, 40, 180, 30), 
						           "Best Score: " + PlayerPrefs.GetInt("BestScore"), swipesLabel);
				}

				/*GUI.Label (new Rect (Screen.width /2 - 100, 70, 200, 30), 
					           "Score: " + score, swipesLabel);
				GUI.Label (new Rect (Screen.width /2 + 10, 70, 200, 30), 
					           "Time Left: " + (30 - (int) typeGame.timer) + "s", swipesLabel);*/

				if(GUI.Button(new Rect(Screen.width/2 - 40, 10, 80, 40), 
				              "Reset", buttonStyle)) {
					reset();
				} 
			}

			if (Input.GetKeyDown(KeyCode.Escape)) {
				if(playGame) {
					playGame = false;
					//typeGame.play = false;
					if(setting == 1 && socket.gotIP) {
						message += (char) 20;
						playGame = false;
						message += "0";
						user.input = message;
						user.send = true;
					}
				} else {
					Application.LoadLevel (0);  
					if(setting ==1) {
						user.input = "" + (char) 28;
						user.send = true;
					}
				}
			}

			//Special Keys
			GUI.Box (new Rect (Screen.width/2 - 110, Screen.height - 135, 215, 120), "", vertGestures);

			
			//R1
			for(int i = 0; i < r1.Length; i++) {
					
				GUI.color = defaultKB;

				if(sixFingers && (!l3 || !r3)) {
					GUI.color = deadKB;
				} else if(activeFingers[0][i] == 1) {
					GUI.color = highlight;
				} else if(activeFingers[0][i] == 2) {
					GUI.color = semiHighlight;
				}

				
				GUI.Box (new Rect(Screen.width/2 - 395 + i*41, Screen.height-100, 37, 40), keys[0].Substring(i, 1), KB);
				
			}
			
			//R2
			for(int i = 0; i < r2.Length; i++) {
				
				GUI.color = defaultKB;
				
				if(sixFingers && (!l3 || !r3)) {
					GUI.color = deadKB;
				} else if(activeFingers[1][i] == 1) {
					GUI.color = highlight;
				} else if(activeFingers[1][i] == 2) {
					GUI.color = semiHighlight;
				} 
				
				GUI.Box (new Rect(Screen.width/2 - 395 + i*41, Screen.height-55, 37, 40), keys[1].Substring(i, 1), KB); 
			}
			
			//L1
			for(int i = 0; i < l1.Length; i++) {
				
				GUI.color = defaultKB;
				
				if(sixFingers && (!l3 || !r3)) {
					GUI.color = deadKB;
				} else if(activeFingers[2][i] == 1) {
					GUI.color = highlight;
				} else if(activeFingers[2][i] == 2) {
					GUI.color = semiHighlight;
				} 
				
				GUI.Box (new Rect(Screen.width/2 + 107 + i*41, Screen.height-100, 37, 40), keys[2].Substring(i,1), KB); 
			}
			
			//L2
			for(int i = 0; i < l2.Length; i++) {
				
				GUI.color = defaultKB;
				
				if(sixFingers && (!l3 || !r3)) {
					GUI.color = deadKB;
				} else if(activeFingers[3][i] == 1) {
					GUI.color = highlight;
				} else if(activeFingers[3][i] == 2) {
					GUI.color = semiHighlight;
				} 
				GUI.Box (new Rect(Screen.width/2 + 107 + i*41, Screen.height-55, 37, 40), keys[3].Substring(i, 1), KB); 
			}



		} else if (Input.GetKeyDown(KeyCode.Escape)) { 
				Application.Quit ();
		}
		
		if(fineTuning) {
			GUI.Box(new Rect(30, 70, (int)(Screen.width * 0.8), (int)(Screen.height * 0.8)), "", config);
			
			GUI.Label (new Rect(70, 70, 200, 70), "Horizontal Threshold: " + hDist.ToString(), swipesLabel);
			hDist = GUI.HorizontalSlider(new Rect(70, 120, 300, 50), hDist, 1.0f, 5.0f);
			
			GUI.Label (new Rect(70, 170, 200, 70), "Vertical Threshold: " + vDist.ToString(), swipesLabel);
			vDist = GUI.HorizontalSlider(new Rect(70, 220, 300, 50), vDist, 1.0f, 5.0f);
			
			GUI.Label (new Rect(70, 270, 250, 70), "Vibration Threshold: " + vibHold.ToString (), swipesLabel);
			vibHold = GUI.HorizontalSlider(new Rect(70, 320, 300, 50), vibHold, 0.1f, 1.0f);

			GUI.Label (new Rect(70, 370, 300, 50), "Vibration", swipesLabel);
			vibrate = GUI.Toggle(new Rect(130, 385, 100, 70), vibrate, "");


			//trailsToggle = GUI.Toggle(new Rect(70, 510, 100, 50), trailsToggle, "");
			//GUI.Label (new Rect(70, 510, 300, 70), "Trails", swipesLabel);

			//sixFingers = GUI.Toggle(new Rect(130, 260, 100, 50), sixFingers, "");
			//GUI.Label (new Rect(150, 430, 300, 70), "6 Finger Mode", swipesLabel);

			if(setting == 1 && socket.gotIP) {
				message += (char) 29;
				if(sixFingers) {
					message += "1";
				} else {
					message += "0";
				}
				user.input = message;
				user.send = true;
				displayOn = GUI.Toggle(new Rect(130, 230, 150, 30), displayOn, "");
				GUI.Label (new Rect(150, 230, 200, 20), "Keyboard Highlights", swipesLabel);
			}

		}
		
	}
	
	string stringReverse(string s) {
		
		if(setting == 1) {
			int len = s.Length;
			char[] reversed = new char[len];
			char[] curr = new char[len];
			curr = s.ToCharArray ();
			
			for(int i = 0; i < len; i++) {
				reversed[i] = curr[len - 1 - i];
			}
			
			string rev = new string(reversed);
			return rev;
		} else {
			return s;
		}
	}

	string capitalize(string s) {
		
		if(s.Length >= 1) {
			char[] orig = s.ToCharArray();
			for(int i = 0; i < orig.Length; i++) {
				if(char.IsLetter(orig[i])) {
					if(char.IsLower(orig[i])) {
						orig[i] = char.ToUpper(orig[i]);
					}
				}
			}
			return new string(orig);
		} else {
			//return illegal argument
			return s;
		}
	}

}


