﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Parse;
using System.Threading.Tasks;

public class GameScript : MonoBehaviour {

	public float timer;
	public float totalTime;
	public float countDown;
	int spawnPeriod;

	ArrayList wordList;
	ArrayList presentWords;
	ArrayList levelWords;
	ArrayList userWords;
	int ran;
	int nWords; //number of words for this level
	int wordN; //word countdown starting with nWords

	public GUIStyle textStyle;
	public GUIStyle buttonStyle;
	public GUIStyle scoreLabel;
	public GUIStyle player;
	public GUIStyle playerW;
	GUIText userInput;
	string typedWord;
	string rawUserInput;

	public AudioClip correct;
	public AudioClip wrong;
	public AudioClip success;
	public AudioClip levelUp;

	public bool play;
	public int charScore;
	public int wordScore;
	public int errorScore;
	Color defaultC;

	MainScript mainCourt;

	public bool hasTarget;
	string[] wordlist;
	Word target;
	Vector3 wordSpeed;
	float playerY;
	int level;
	int maxLevel;

	// Use this for initialization
	void Start () {

		//initial setting
		totalTime = 0;
		timer = 0;
		countDown = 0;
		spawnPeriod = 2;
		nWords = 10;	//number of words player has to type
		wordN = nWords;	//countdown of words
		charScore = 0;
		wordScore = 0;
		errorScore = 0;
		playerY = Screen.height/2;
		level = 0;
		maxLevel = 6;

		TextAsset wordfile = Resources.Load(level.ToString ()) as TextAsset;
		wordlist = wordfile.text.Split('\n');


		//create first word
		presentWords = new ArrayList ();

		levelWords = new ArrayList ();
		userWords = new ArrayList ();

		GameObject g = GameObject.Find ("Output");
		mainCourt = g.GetComponent<MainScript>();
		userInput = g.GetComponent<GUIText> ();
		typedWord = userInput.text;

		play = false;
		defaultC = Color.Lerp (Color.gray, Color.black, 0.8f);
		hasTarget = false;
		wordSpeed = new Vector3 (-0.3f, 0, 0);
	}

	void spawnWord() {
		ran = (int)(Random.value * wordlist.Length);
		Word newWord = new Word (wordlist [ran]);
		newWord.velocity = wordSpeed;
		presentWords.Add (newWord);
	}

	void printInfo() {
		string filePath = "./" + mainCourt.playerName + " " + 
			"Level " + level + System.DateTime.UtcNow.ToString(" MMMM,dd,yyyy HH.mm") + ".txt";
		string gameData = "";
		
		gameData += "Words/min: " + wordScore/timer * 60.0f + '\n';
		gameData += "Characters/min: " + charScore/timer * 60.0f + '\n';
		gameData += "Time to finish this level: " + Mathf.FloorToInt(timer)/60 + "min " 
						+ Mathf.FloorToInt(timer)%60 + "sec" + '\n';
		gameData += "Total mistakes: " + errorScore + '\n';
		gameData += "Total words: " + nWords + '\n';
		gameData += "Total words typed: " + wordScore + '\n';
		gameData += "Total characters typed: " + charScore + '\n';
		gameData += "Total time played: " + Mathf.FloorToInt(totalTime)/60 + "min " 
						+ Mathf.FloorToInt(totalTime)%60 + "sec" +'\n';
		gameData += "===========================================" + '\n';
		gameData += "Target Word" + '\t' + "User's Input" + '\n';

		for(int i =0 ;i < userWords.Count; i++) {
			gameData += " " + levelWords[i]  + '\t' + '\t' + userWords[i] + '\n';
		}
		
		//Write the coords to a file
		System.IO.File.WriteAllText(filePath,gameData);

		var user = ParseUser.CurrentUser;

		var session = new ParseObject("Session");
		session["level"] = level;
		session["wordsPerMin"] = wordScore/timer * 60.0f;
		session["charactersPerMin"] = charScore/timer * 60.0f;
		session["finishTime"] = timer;
		session["totalMistake"] = errorScore;
		session["totalWords"] = nWords;
		session["totalWordsTyped"] = wordScore;
		session["totalCharactersTyped"] = charScore;

		session.SaveAsync().ContinueWith(t => {

			if (t.IsCanceled)
			{
				// the save was cancelled.
				Debug.Log ("task was cancelled");
			}
			else if (t.IsFaulted)
			{
				Debug.Log ("Faulted");
				foreach(var e in t.Exception.InnerExceptions) {
					ParseException parseException = (ParseException) e;
					Debug.Log("Error message " + parseException.Message);
					Debug.Log("Error code: " + parseException.Code);
				}
			}
			else {
				var relation = user.GetRelation<ParseObject>("Sessions");
				relation.Add (session);
				Task saveTask = user.SaveAsync();
			}
		});
	}
	
	// Update is called once per frame
	void Update () {
		if(play) {

			//game is over
			if(wordN <= 0) {
				AudioSource.PlayClipAtPoint(levelUp, gameObject.transform.position);
				printInfo();
				nextGame();
			}

			//spawn every few seconds or when there are no words
			if(countDown > spawnPeriod || presentWords.Count == 0) {
				countDown = 0;
				spawnWord ();
			}

			//when stored TypedWord and input are not the same,
			//it means that a new char was typed or a char was deleted
			if(typedWord != userInput.text && hasTarget && userInput.text != "") {

				rawUserInput += userInput.text.Substring(userInput.text.Length-1, 1);
				//truncate if too many letters were typed
				if(userInput.text.Length > target.value.Length) {
					userInput.text = userInput.text.Substring(0, target.value.Length);
				}

				if(userInput.text == target.value.Substring(0, userInput.text.Length)) {
					//play correctSound
					AudioSource.PlayClipAtPoint(correct, gameObject.transform.position );

				} else {
					//play wrongSound
					errorScore += 1;
					AudioSource.PlayClipAtPoint(wrong, gameObject.transform.position );
				}
			}

			if(!hasTarget) {
				//find a target
				foreach (Word w in presentWords) {
					if(userInput.text != "" &&
					   userInput.text.Substring(0,1) == w.value.Substring(0,1)) {
						//found target
						w.target = true;
						hasTarget = true;
						target = w;
						playerY = target.position.y - 50;
						AudioSource.PlayClipAtPoint(correct, gameObject.transform.position );
						break;
					}
				}
				//if still none
				if(!hasTarget && userInput.text.Length > 0) {
					AudioSource.PlayClipAtPoint(wrong, gameObject.transform.position );
					userInput.text = "";
				}
			} else {
				//if input and target are same
				if(userInput.text == target.value && userInput.text != "") {

					charScore += target.value.Length;
					if(target.value.Length > 1 ) {
						wordScore += 1;
						AudioSource.PlayClipAtPoint(success, gameObject.transform.position);
					}

					levelWords.Add (target.value);
					userWords.Add(rawUserInput);
					rawUserInput = "";
					
					userInput.text = "";
					target.target = false;
					target.typed = true;
					hasTarget = false;

				} else {

					//keep track of correctly Typed
					int track = 0;
					for(int i = 0; i < userInput.text.Length; i++) {
						if(target.value.Substring(i,1) == userInput.text.Substring(i,1)) {
							track = i;
						} else {
							break;
						}
					}

					if(userInput.text != "") {
						userInput.text = userInput.text.Substring(0,track + 1); 
					}
				}
			}

			//remove invalid words
			int cnt = presentWords.Count;
			if(cnt > 0) {
				for(int i = cnt - 1; i >= 0 ; i--) {
					if(!((Word)presentWords[i]).valid) {
						if(((Word)presentWords[i]).target) {
							hasTarget = false;
							userInput.text = "";
						}
						presentWords.RemoveAt(i);
						wordN--;
					}
				}
			}

			foreach (Word w in presentWords) {
				w.Update();
			}

			timer += Time.deltaTime;
			countDown += Time.deltaTime;
			totalTime += Time.deltaTime;
			mainCourt.score = charScore + wordScore;

			//get typedWord from userInput
			typedWord = userInput.text;
	
		} else {
			mainCourt.gameToggle = false;
		}
	}

	public void reset() {

		level = 0;
		TextAsset wordfile = Resources.Load(level.ToString()) as TextAsset;
		wordlist = wordfile.text.Split('\n');
		
		//initial setting
		timer = 0;
		countDown = 0;
		spawnPeriod = 3;
		nWords = 10;	//number of words player has to type
		wordN = nWords;
		charScore = 0;
		wordScore = 0;
		errorScore = 0;
		
		//create first word
		presentWords = new ArrayList ();
		
		play = true;
		defaultC = Color.Lerp (Color.gray, Color.black, 0.8f);
		hasTarget = false;
		wordSpeed = new Vector3 (-0.3f, 0, 0);
		playerY = Screen.height/2;
	}

	void nextGame() {

		level++;
		if(level > maxLevel) {
			level = 0;
		}

		TextAsset wordfile = Resources.Load(level.ToString()) as TextAsset;
		wordlist = wordfile.text.Split('\n');
		presentWords = new ArrayList ();
		levelWords = new ArrayList ();
		userWords = new ArrayList ();

		rawUserInput = "";

		countDown = 0;
		nWords = 10;	//number of words player has to type
		wordN = nWords;
		charScore = 0;
		wordScore = 0;
		errorScore = 0;
		timer = 0;

		defaultC = Color.Lerp (Color.gray, Color.black, 0.8f);
		hasTarget = false;
	}


	void OnGUI() {

		if(play) {

			if(GUI.Button (new Rect(Screen.width * (6.0f/7.0f), 10, 140, 50), 
			               "Pause", buttonStyle)) {
				play = false;
			}

			if(GUI.Button (new Rect(Screen.width * (6.0f/7.0f), 70, 140, 50), 
			              "Stop", buttonStyle)) {
				printInfo();
				reset ();
				play = false;
			}

			GUI.Label (new Rect (Screen.width/2 - 100, 10, 200, 30), 
			           "Score: " + charScore, scoreLabel);
			GUI.Label (new Rect(Screen.width/2, 10, 200, 30), 
			           "Words/min: " + (int)(wordScore/timer * 60.0f), scoreLabel);


			if (Event.current.type == EventType.KeyUp ) {
				if(Event.current.keyCode == KeyCode.UpArrow) {
					wordSpeed.x *= 1.5f;
				} else if (Event.current.keyCode == KeyCode.DownArrow) {
					wordSpeed.x *= 0.5f;
				}
				foreach (Word w in presentWords) {
					w.velocity = wordSpeed;
				}

			}

			if(hasTarget) {
				GUI.Box (new Rect (-20, playerY, 120, 120), "", player);
			} else {
				GUI.Box (new Rect (-20, playerY, 120, 120), "", playerW);
			}
			
			if(presentWords.Count > 0 ) {
				foreach (Word w in presentWords) {
					//keep track of x position to center correctly
					float pos = textStyle.CalcSize(new GUIContent(w.value)).x;
					GUI.color = defaultC;

					if(w.valid) {
						GUI.Label (new Rect(w.position.x - pos/2.0f, w.position.y, pos, 30),
						           w.value, textStyle);
						if(w.typed) {
							GUI.color = Color.green;
							GUI.Label (new Rect(w.position.x - pos/2.0f, w.position.y, pos, 30),
							           w.value, textStyle);
						}
					}

				}
			}
				
			//look for correctly typed letters
			if(hasTarget && target.valid) {
				float pad = 0;
				float posX = textStyle.CalcSize(new GUIContent(target.value)).x;
				for(int i = 0; i < target.value.Length; i++) {
					if(i > 0) {
						pad += (textStyle.CalcSize(new GUIContent(target.value.Substring(i-1, 1)))).x;
					}
							
					if(userInput.text.Length > 0 && userInput.text.Length > i) {
						//keep track where to write
						if(target.value.Substring(i,1) == userInput.text.Substring(i,1)) {
							//yellow letters only when user have any input and is correct
							GUI.color = Color.yellow;
							GUI.Label (new Rect(target.position.x - posX/2.0f + pad, target.position.y, 30, 30),
									           target.value.Substring(i, 1), textStyle);

						}
					} 
				}
			}
		}
	}
}
