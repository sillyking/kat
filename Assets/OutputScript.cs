﻿using UnityEngine;
using System.Collections;

public class OutputScript : MonoBehaviour {

	GlobalScript globalObj;
	GUIText outputText;
	string current;

	int screenW;
	int screenH;
	Vector2 scrollPosition;
	Control socket;

	public GUIStyle typeStyle;

	// Use this for initialization
	void Start () {
		GameObject g = GameObject.Find ("GlobalObject");
		globalObj = g.GetComponent<GlobalScript> ();
		outputText = gameObject.GetComponent<GUIText> ();
		current = "";
		screenW = Screen.width;
		screenH = Screen.height;

		GameObject gg = GameObject.Find ("Sockets");
		socket = gg.GetComponent<Control> ();
	}
	
	// Update is called once per frame
	void Update () {
		if(!globalObj.playGame) {
			current = globalObj.output;
		} else {
			globalObj.output = outputText.text;
		}
	}


	void OnGUI() {
		if(socket.gotIP) {
			GUILayout.BeginArea(new Rect((int)(screenW * 0.15), (int)(screenH * 0.15), 
			                             (int)(screenW * 0.85), (int)(screenH * 0.65)));

			int width = (int)(screenW * 0.68);
			int height = (int)(screenH * 0.49);
			scrollPosition = GUILayout.BeginScrollView(scrollPosition, false, false,
			                                           GUILayout.Width(width), 
			                                           GUILayout.Height (height));

			GUILayout.Label (current + "_", typeStyle);
			

			GUILayout.EndScrollView ();
			GUILayout.EndArea();
		}
		
	}
}
