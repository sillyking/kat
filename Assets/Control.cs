﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;


public class Control : MonoBehaviour
{
	
	const int
		kPort = 42209,
		kHostConnectionBacklog = 10;
	

	static Control instance;
	
	public bool isServer;
	public bool gotIP = false;
	string message = "Awake";
	string clientIP = "";
	Socket socket;
	IPAddress ip;
	List<Socket> m_socketList;

	public GUIStyle buttonStyle;
	public GUIStyle labelStyle;
	
	static Control Instance {
		get {
			if (instance == null) {
				instance = (Control)FindObjectOfType (typeof (Control));
			}
			return instance;
		}
	}
	
	
	public static Socket Socket {
		get {
			return Instance.socket;
		}
	}
	
	
	void Start (){
		Application.RegisterLogCallbackThreaded (OnLog);	
		isServer = true;
		m_socketList = new List<Socket> ();

		if (isServer){
			if (Host (kPort)){
				gameObject.SendMessage ("OnServerStarted");
			}
		}	
	}

	void Update()
	{
		if (m_socketList.Count > 0) {
						gameObject.SendMessage ("OnClientConnected", m_socketList [0]);
						m_socketList.RemoveAt(0);
				}
	}

	
	void OnApplicationQuit () {
		Disconnect ();
	}
	
	
	public bool Host (int port){
		Debug.Log ("Hosting on port " + port);
		
		socket = new Socket (AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
		
		try
		{
			socket.Bind (new IPEndPoint (IP, port));
			socket.Listen (kHostConnectionBacklog);
			socket.BeginAccept (new System.AsyncCallback (OnClientConnect), socket);
		}
		catch (System.Exception e)
		{
			Debug.LogError ("Exception when attempting to host (" + port + "): " + e);
			
			socket = null;
			
			return false;
		}
		
		return true;
	}
	
	
	public bool Connect (IPAddress ip, int port){
		Debug.Log ("Connecting to " + ip + " on port " + port);
		
		socket = new Socket (AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
		socket.Connect (new IPEndPoint (ip, port));
		
		if (!socket.Connected)
		{
			Debug.LogError ("Failed to connect to " + ip + " on port " + port);
			
			socket = null;
			return false;
		}

		return true;
	}
	
	
	public void Disconnect (){
		if (socket != null)
		{
			socket.BeginDisconnect (false, new System.AsyncCallback (OnEndHostComplete), socket);
		}
	}
	
	
	void OnClientConnect (System.IAsyncResult result) {
		Debug.Log ("Handling client connecting");
		
		try
		{
			//gameObject.SendMessage ("OnClientConnected", socket.EndAccept (result));
			m_socketList.Add (socket.EndAccept(result));
		}
		catch (System.Exception e)
		{
			Debug.LogError ("Exception when accepting incoming connection: " + e);
		}
		
		try
		{
			socket.BeginAccept (new System.AsyncCallback (OnClientConnect), socket);
		}
		catch (System.Exception e)
		{
			Debug.LogError ("Exception when starting new accept process: " + e);
		}
	}

	
	void OnEndHostComplete (System.IAsyncResult result){
		socket = null;
	}
	
	//Get the IP address of the machine on which the code is running
	public IPAddress IP{
		get
		{
			if (ip == null)
			{
				ip = (
					from entry in Dns.GetHostEntry (Dns.GetHostName ()).AddressList
					where entry.AddressFamily == AddressFamily.InterNetwork
					select entry
					).FirstOrDefault ();
			}
			
			return ip;
		}
	}
	
	
	void OnGUI (){

		//for debugging
		//GUILayout.Label (message);

		if(!isServer && !gotIP){
			if(GUI.Button(new Rect(Screen.width - 100, 10, 80, 40), "Exit", buttonStyle)) {
				Application.Quit ();
			}
			if(GUI.Button(new Rect(10, 10, 220, 40), "Stand Alone Demo", buttonStyle)) {
				//make GameScript setting to 0
				GameObject g = GameObject.Find ("GlobalObject");
				GlobalScript gameS = g.GetComponent<GlobalScript> ();
				gameS.setting = 0;
				gameS.modeUpdate();
				gotIP = true;
			}

			//check size of savedIPs
			string ipKey = "SavedIP";
			int cnt = 0;
			while(PlayerPrefs.HasKey(ipKey + cnt.ToString())){
				cnt++;
			}

			if(cnt > 0) {
				GUI.Label (new Rect(Screen.width/2 - 250, Screen.height/2 - 200, 200, 30),  
				           "Previously used IP: ", labelStyle);
			}

			for(int i = 0; i < cnt; i++) {
				if(GUI.Button(new Rect(Screen.width/2 - 250, Screen.height/2 - 170 + i*45, 200, 40), 
				              PlayerPrefs.GetString(ipKey + i.ToString ()), buttonStyle)){

					if (Connect (IPAddress.Parse (PlayerPrefs.GetString(ipKey + i.ToString ())), kPort))
					{
						gameObject.SendMessage ("onClientStarted", socket);
						gotIP = true;
					}

				}
			}
				
			if(GUI.Button(new Rect(Screen.width/2 + 30, Screen.height/2 - 120, 200, 40), "Submit IP", buttonStyle)) {
				Event.current.Use ();	
				if (Connect (IPAddress.Parse (clientIP), kPort))
				{
					gameObject.SendMessage ("onClientStarted", socket);
					PlayerPrefs.SetString("SavedIP" + cnt, clientIP);
					gotIP = true;
									
				}
			}

			GUI.Label (new Rect(Screen.width/2 + 30, Screen.height/2 - 200, 200, 30),  
			           "New IP: ", labelStyle);
			clientIP = GUI.TextArea(new Rect(Screen.width/2 + 30, Screen.height/2 - 170, 200, 30), clientIP);
		}

	}

	void OnLog (string message, string callStack, LogType type)
	{
		this.message = message + "\n" + this.message;
	}
}