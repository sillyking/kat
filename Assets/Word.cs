﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
	
public class Word  {
		
	public string value;
	public Vector3 position;
	public Vector3 velocity;
	public GUIStyle textStyle;
	public int markCorrect;
	public bool target;
	public bool valid;
	public bool typed;
	
	float timer;
		
	public Word(string s){
		value = s;
		int ran = (int)(Random.value * 6.0f);
		position = new Vector3(Screen.width * 0.8f, (ran + 1)*(Screen.height * 0.6f)/7.0f, 0.0f);
		timer = 0.0f;
		velocity = new Vector3(-0.4f, 0, 0);
		markCorrect = 0;
		target = false;
		valid = true;
		typed = false;
	}
	

	public void Update() {

		if( position.x <= Screen.width * 0.05) {
			valid = false;
		}

		position += velocity;

		//timer for typed words
		if(typed) {
			timer += Time.deltaTime;
		}

		if(timer > 1) {
			valid = false;
		}
	
	}
	
}
	
	

