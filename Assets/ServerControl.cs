﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;


public class ServerControl : MonoBehaviour
{
	public List<Socket> clients = new List<Socket> ();
	public string message  = "";
	
	
	void OnServerStarted ()
	{
		Debug.Log ("Server started");
		//broadcast the IP to any machine listening so that it can connect
	}
	
	
	void OnClientConnected (Socket client)
	{
		Debug.Log ("Client connected");
		clients.Add (client);
		SocketRead.Begin (client, OnReceive, OnReceiveError);
	}
	
	
	void OnReceive (SocketRead read, byte[] data)
	{
		//string message = "Client " + clients.IndexOf (read.Socket) + " says: " + Encoding.ASCII.GetString (data, 0, data.Length);
		//Debug.Log (message);

		/*
		GameObject g = GameObject.Find ("Output");
		//for game
		MainScript output = g.GetComponent<MainScript> ();*/
		message += Encoding.ASCII.GetString (data, 0, data.Length);
		
		foreach (Socket client in clients)
		{
			if (client == read.Socket)
			{
				continue;
			}
			
			client.Send (Encoding.ASCII.GetBytes (message));
		}
	}
	
	
	void OnReceiveError (SocketRead read, System.Exception exception)
	{
		Debug.LogError ("Receive error: " + exception);
	}

	void OnGUI() {

	}


}