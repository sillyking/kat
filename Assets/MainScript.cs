﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Parse;

public class MainScript : MonoBehaviour {

	GUIText outputText;
	public string message;
	
	public GUIStyle KB;
	public GUIStyle swipesLabel;
	public GUIStyle optLabel;
	public GUIStyle buttS;

	public GUIStyle vertGestures;
	public GUIStyle colBox;
	public GUIStyle bgBox;

	public GUIStyle gestureL;
	public GUIStyle gestureM;
	public GUIStyle gestureR;
	
	Color deadKB;
	Color defaultKB;
	Color semiHighlight;
	Color highlight;
	Color leftKB;
	Color rightKB;
	
	int keyCount;	//7 keys
	int[][] activeFingers;
	string[] keys;
	
	string l1;
	string l2;
	string r1;
	string r2;

	string name = "";
	string password = "";
	
	bool l3,r3;
	
	bool sixFingers;
	public int score;
	
	string mode;
	ServerControl server;
	Control control;

	GameScript typeGame;
	public bool gameToggle;
	public bool parseLogin;
	
	public Dictionary<int, GameObject> trails;
	bool legendToggle;
	bool trailToggle;
	public string playerName;

	void Awake() {
		Application.targetFrameRate = 20;
	}
	
	// Use this for initialization
	void Start () {
		message = "";
		outputText = gameObject.GetComponent<GUIText> ();

		deadKB = Color.Lerp (Color.black, Color.gray, 0.5f);
		defaultKB = Color.Lerp (Color.white, Color.gray, 0.6f);
		semiHighlight = Color.Lerp (Color.red, Color.yellow, 0.3f);
		highlight = Color.yellow;
		leftKB = new Color (0.0f, 82.0f / 255, 143.0f / 255); 
		rightKB = new Color (0.0f, 141.0f / 255, 143.0f / 255); 

		keyCount = 7;
		
		mode = "alpha";	
		l1 = "slue,qc";
		l2 = "idgrxvk";
		r1 = "bw.aomt";
		r2 = "yjznfph";
		
		trails = new Dictionary<int, GameObject> ();	
		keys = new string[4] {r1, r2, l1, l2};
		activeFingers = new int[4][] {new int[keyCount], new int[keyCount], 
			new int[keyCount], new int[keyCount]};

		for(int i = 0; i < 4; i++) {
			for(int j = 0; j < keyCount; j++) {
				activeFingers[i][j] = 0;
			}
		}

		GameObject g = GameObject.Find ("Sockets");
		control = g.GetComponent<Control> ();
		server = g.GetComponent<ServerControl> ();
		GameObject ga = GameObject.Find ("Word");
		typeGame = ga.GetComponent<GameScript> ();

		gameToggle = false;
		score = 0;
		legendToggle = false;
		trailToggle = false;
		parseLogin = false;

		if (PlayerPrefs.HasKey ("PlayerName")) {
			playerName = PlayerPrefs.GetString("PlayerName");
		} else {
			playerName = "";
		}
	}

	void modeUpdate() {

		string l1New = l1;
		string l2New = l2;
		string r1New = r1;
		string r2New = r2;

		switch(mode) {
		case "alpha":
			l1New = l1;
			l2New = l2;
			r1New = r1;
			r2New = r2;
			break;
		case "CAPSLock":
			l1New = capitalize(l1);
			l2New = capitalize(l2);
			r1New = capitalize(r1);
			r2New = capitalize(r2);
			break;
		case "numLock":
			l1New = "12345+-";
			l2New = "67890*/";
			r1New = "=|%()[]";
			r2New = "!#<>^:;";
			break;
		case "specialLock":
			l1New = ".,?!@#$";
			r1New = "~*|()-_";
			r2New = "+=%[]^&";
			l2New = "\"'`{}\\/";
			break;
		}
		
		keys [0] = r1New;
		keys [1] = r2New;
		keys [2] = l1New;
		keys [3] = l2New;
	}


	// Update is called once per frame
	void Update () {

		message = server.message;
		server.message = "";

		if(outputText.text.Length > 0 && outputText.text.Length % 45 == 0) {
			outputText.text += '\n';
		}
		
		int currentLength = message.Length;
		while(currentLength > 0) {

			if(outputText.text.Length > 0 && outputText.text.Length % 45 == 0) {
				outputText.text += '\n';
			}

			switch(message[0]) {
			
			//game toggle
			case ((char)17):
				if(message.Substring(1, 1) == "0") {
					typeGame.play = false;
					gameToggle = false;
				} else if(message.Substring(1, 1) == "1" && server.clients.Count > 0) {
					if(!gameToggle) {
						gameToggle = true;
						startGame();
					}
				}
				currentLength = currentLength - 2;
				message = message.Substring(2, currentLength);
				break;
			
			//toggle for Legend
			case (char)20:
				if(message.Substring(1, 1) == "1") {
					legendToggle = true;
				} else if(message.Substring(1, 1) == "0") {
					legendToggle = false;
				}
				currentLength = currentLength - 2;
				message = message.Substring(2, currentLength);
				break;
		
			//update output 
			case ((char) 21):
				if(!legendToggle){
					getMappedKey(message.Substring(1, 3));
				} 
				if(currentLength >= 4) {
					currentLength -= 4;
					message = message.Substring(4, currentLength);
				}
				break;	
			
			//update keyboardGUI 
			case ((char) 22): 		
				updateActive(message.Substring(1, 3));
				if(currentLength >= 4) {
					currentLength -= 4;
					message = message.Substring(4, currentLength);
				}
				break;
			
			//refresh Keyboard
			case ((char) 23):
				refreshKeyBoard();
				if(currentLength >= 1) {
					currentLength--;
					message = message.Substring(1, currentLength);
				}
				break;
			
			//reset
			case ((char) 24):
				if(currentLength >= 1) {
					currentLength--;
					message = message.Substring(1, currentLength);
				} 
				reset ();
				break;
			
			//draw trails (25:ID, 26:Pos, 27:Delete)
			case ((char) 25):
				int trailID = int.Parse(message.Substring(1,1));
				currentLength -= 2;
				
				if(message[2] == (char) 27) {
					if (trails.ContainsKey(trailID)) {
						GameObject trail = trails[trailID];
						Destroy (trail, trail.GetComponent<TrailRenderer>().time);
						trails.Remove(trailID);
					}
					currentLength--;
					message = message.Substring (3, currentLength);
				} else {
					message = message.Substring(2, currentLength);
					string x = "";
					string y = "";
					
					while(message[0] != (char) 26) {
						x += message[0];
						currentLength--;
						message = message.Substring(1, currentLength);
					}
					currentLength--;
					message = message.Substring(1, currentLength);
					
					while(message[0] != (char) 26) {
						y += message[0];
						currentLength--;
						message = message.Substring(1, currentLength);
					}
					currentLength--;
					message = message.Substring(1, currentLength);
					
					Vector3 oldPos = new Vector3(float.Parse(x), float.Parse(y), 0.0f);
					Vector3 temp = Camera.main.WorldToScreenPoint(oldPos);
					Vector3 position = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width - temp.x, temp.y, temp.z));
					
					if(!trails.ContainsKey(trailID)) {
						//make trail
						if(trailToggle) {
							GameObject trail = SpecialEffects.MakefTrail(position);
							trails.Add (trailID, trail);
						}

					} else {
						//move trail (if difference is too big might be another finger)
						GameObject trail = trails[trailID];
						if(Mathf.Abs(trail.transform.position.x - position.x) <= 10.0f
						   && Mathf.Abs (trail.transform.position.y - position.y) <= 10.0f) {
							if(trailToggle) {
								trail.transform.position = position;
							}
						} else {
							trails.Remove(trailID);
						}
					}
				}
				break;
			
			//clientside exit
			case (char)28:
				server.clients.RemoveAt(server.clients.Count - 1);
				currentLength--;
				message = message.Substring(1, currentLength);
				reset();
				if(gameToggle) {
					gameToggle = false;
					typeGame.play = false;
				}
				break;
			
			//six finger mode toggle
			case (char)29:
				if(message.Substring(1, 1) == "0") {
					sixFingers = false;
				} else if(message.Substring(1, 1) == "1") {
					sixFingers = true;
				}
				currentLength = currentLength - 2;
				message = message.Substring(2, currentLength);
				break;

			//l3 for six finger mode
			case (char)30:
				if(message.Substring(1, 1) == "0") {
					l3 = false;
				} else if(message.Substring(1, 1) == "1") {
					l3 = true;
				}
				currentLength = currentLength - 2;
				message = message.Substring(2, currentLength);
				break;

			//r3 for six finger mode
			case (char)31:
				if(message.Substring(1, 1) == "0") {
					r3 = false;
				} else if(message.Substring(1, 1) == "1") {
					r3 = true;
				}
				currentLength = currentLength - 2;
				message = message.Substring(2, currentLength);
				break;
		}

		if(gameToggle) {
			//game is over
			if(typeGame.play == false) {
				gameToggle = false;
			}
		}
	}
	}
	
	void updateActive(string s) {
		
		//receive message here should include currentFinger, gestures
		//if s has length 2 then one transition, if 3 then 2 transitions
		
		int currentFinger = int.Parse(s.Substring (0, 1));
		
		
		if(currentFinger < 4) {
			if(s.Substring(2,1) == "_") {
				switch(s.Substring (1,1)) {
				case "|":
					activeFingers[currentFinger][4] = 2;
					activeFingers[currentFinger][3] = 1;
					activeFingers[currentFinger][2] = 2;
					break;
				case ">":
					activeFingers[currentFinger][0] = 1;
//					activeFingers[currentFinger][1] = 2;
					break;
				case "<":
//					activeFingers[currentFinger][5] = 2;
					activeFingers[currentFinger][6] = 1;
					break;
				case "^":
					break;
				case "v":
					break;
				}
			} else if(s.Length == 3) {
				switch (s.Substring (1,1)) {
				case "|":
					switch (s.Substring (2,1)) {
					case "-":
						activeFingers[currentFinger][3] = 1;
						break;
					case ">":
						activeFingers[currentFinger][2] = 1;
						break;
					case "<": 
						activeFingers[currentFinger][4] = 1;
						break;
					case "^":
						break;
					case "v":
						break;
					}
					break;
				case ">":
					switch (s.Substring (2,1)) {
					case "-":
						activeFingers[currentFinger][0] = 1;
						break;
					case "|": 
						activeFingers[currentFinger][1] = 1;
						break;
					} 
					break;
				case "<":
					switch (s.Substring (2,1)) {
					case "-":
						activeFingers[currentFinger][6] = 1;
						break;
					case "|": 
						activeFingers[currentFinger][5] = 1;
						break;
					} 
					break;
				case "^":
					break;
				case "v":
					break;
				}
			}
		}
	}
	
	void getMappedKey(string s) {
		
		//Printing Mapped Characters
		int currentFinger = int.Parse(s.Substring (0, 1));

		if(currentFinger < 4) {
			switch (s.Substring (1, 1)) {
			case "|":
				switch (s.Substring (2, 1)) {
				case "-":
					outputText.text += keys[currentFinger].Substring(3, 1);
					break;
				case ">":
					outputText.text += keys[currentFinger].Substring(2, 1);
					break;
				case "<": 
					outputText.text += keys[currentFinger].Substring(4, 1);
					break;
				}
				modeUpdate();
				break;
			case ">":
				switch (s.Substring (2, 1)) {
				case "-":
					outputText.text += keys[currentFinger].Substring(0, 1);
					break;
				case "|": 
					outputText.text += keys[currentFinger].Substring(1, 1);
					break;
				} 
				modeUpdate();
				break;
			case "<":
				switch (s.Substring (2, 1)) {
				case "-":
					outputText.text += keys[currentFinger].Substring(6, 1);
					break;
				case "|": 
					outputText.text += keys[currentFinger].Substring(5, 1);
					break;
				} 
				modeUpdate();
				break;
			case "^":
				switch (s.Substring (2, 1)) {
				case "-": 
					//right hand Up Swipe
					if(currentFinger == 0 || currentFinger ==1) {
						if(outputText.text.Length >= 1 ) {
						outputText.text = outputText.text.Substring(0, outputText.text.Length-1);
						}
					} else {
						if(mode != "numLock") {
							mode = "numLock";
							modeUpdate();
						} else {
							mode = "alpha";
							modeUpdate();
						}
					}
					modeUpdate();
					break;
				case "|":
					//right hand Up Hold
					if(currentFinger == 0 || currentFinger ==1) {
						//clear
						reset();
					} else {
						//left hand Up Hold
						if(mode != "specialLock") {
							mode = "specialLock";
							modeUpdate();
						} else {
							mode = "alpha";
							modeUpdate();
						}
					}
					break;
				}
				break;
			case "v":
				switch (s.Substring (2, 1)) {
				case "-": 
					//right hand Down Swipe
					if(currentFinger == 0 || currentFinger ==1) {
						outputText.text += " ";
					} else {
						//left hand Down Swipe
						if(mode != "CAPSLock") {
							keys[2] = capitalize(l1);
							keys[3] = capitalize(l2);
							keys[0] = capitalize(r1);
							keys[1] = capitalize(r2);
						} else {
							keys[2] = l1;
							keys[3] = l2;
							keys[0] = r1;
							keys[1] = r2;
						}
					}
					break;
				case "|":
					//right hand Down Hold
					if(currentFinger == 0 || currentFinger ==1) {
						outputText.text += char.ToString('\n');
					} else {
						//left hand Down Hold
						if(mode != "CAPSLock") {
							mode = "CAPSLock";
							modeUpdate();
						} else {
							mode = "alpha";
							modeUpdate();
						}
					}
					break;
				}
				break;
			}
		} else if(currentFinger < 6) {
			switch (s.Substring (1, 1)) {
			case "<":
				switch (s.Substring (2, 1)) {
				case "-": 
					//right hand Up Swipe
					if(currentFinger == 4) {
						outputText.text += " ";
					}
					modeUpdate();
					break;
				case "|":
					//right hand Up Hold
					if(currentFinger == 4) {
						if(mode != "numLock") {
							mode = "numLock";
							modeUpdate();
						} else {
							mode = "alpha";
							modeUpdate();
						}
					} else {
						//left hand Up Hold
						if(mode != "specialLock") {
							mode = "specialLock";
							modeUpdate();
						} else {
							mode = "alpha";
							modeUpdate();
						}
					}
					break;
				}
				break;
			case ">":
				switch (s.Substring (2, 1)) {
				case "-": 
					//right hand Down Swipe
					if(currentFinger == 4) {
						outputText.text = outputText.text.Substring(0, outputText.text.Length-1);
					} else {
						//left hand Down Swipe
						if(mode != "CAPSLock") {
							keys[0] = capitalize(r1);
							keys[1] = capitalize(r2);
							keys[2] = capitalize(l1);
							keys[3] = capitalize(l2);
						} else {
							keys[0] = r1;
							keys[1] = r2;
							keys[2] = l1;
							keys[3] = l2;
						}
					}
					break;
				case "|":
					//right hand Down Hold
					if(currentFinger == 4) {
						outputText.text += char.ToString('\n');
					} else {
						//left hand Down Hold
						if(mode != "CAPSLock") {
							mode = "CAPSLock";
							modeUpdate();
						} else {
							mode = "alpha";
							modeUpdate();
						}
					}
					break;
				}
				break;
			}
		}
	}
	
	void refreshKeyBoard() {
		activeFingers = new int[4][] {new int[keyCount], new int[keyCount], 
			new int[keyCount], new int[keyCount]};
	}
	
	void reset() {
		keyCount = 7;
		outputText.text = "";
		refreshKeyBoard();
		mode = "alpha";
		modeUpdate();
	}

	string capitalize(string s) {
		
		if(s.Length >= 1) {
			char[] orig = s.ToCharArray();
			for(int i = 0; i < orig.Length; i++) {
				if(char.IsLower(orig[i])) {
					orig[i] = char.ToUpper(orig[i]);
				}
			}
			return new string(orig);
		} else {
			//return illegal argument
			return s;
		}
	}

	void startGame() {
		reset ();
		score = 0;
		typeGame.play = true;
		gameToggle = true;
	}

	void logIn() {
		ParseUser.LogInAsync (name, password).ContinueWith (t => {
			if (t.IsFaulted || t.IsCanceled) {
				// The login failed. Check the error to see why.
			} else {
				// Login was successful.
				parseLogin = true;
				Debug.Log ("successful login");

			}
		});
	}

	
	void OnGUI() {

		float keySize = 38.0f;
		float keyPad = 2.0f;
		float legendWidth = 20;
		if (legendToggle) {
			legendWidth = 250;
		}

		//reset game when hit enter
		if (Event.current.type == EventType.KeyUp 
		    && Event.current.keyCode == KeyCode.Return) {
			control.Disconnect();
			Application.LoadLevel(Application.loadedLevel);
		}

		if(!gameToggle) {

			if(server.clients.Count > 0) {
				GUI.color = Color.white;
				GUI.Label (new Rect(Screen.width - 150, 10, 150, 30), "CONNECTED!",
				           swipesLabel);
			} else {
				GUI.color = Color.Lerp(Color.gray, Color.black, 0.8f);
				GUI.Label (new Rect(Screen.width - 150, 10, 150, 30), "NO CONNECTION",
				           swipesLabel);
			}
		
			if (!parseLogin) {
				if(PlayerPrefs.HasKey("UserName")) {
					GUI.color = Color.white;
					name = PlayerPrefs.GetString("UserName");
					password = PlayerPrefs.GetString ("Password");
					logIn();

				} else {
					GUI.color = Color.white;
					//Enter User Name
					GUI.Label (new Rect(Screen.width/2 - 320, Screen.height/2 - 150, 400, 30), 
					           "User Name", optLabel);
					name = GUI.TextArea(new Rect(Screen.width/2 - 10 , Screen.height/2 - 145, 300, 30), name);

					//Enter Password
					GUI.Label (new Rect(Screen.width/2 - 320, Screen.height/2 - 100, 400, 30), 
					           "Password", optLabel);
					password = GUI.TextArea(new Rect(Screen.width/2 - 10, Screen.height/2 - 95, 300, 30), password);

					if(GUI.Button(new Rect(Screen.width/2 - 200, Screen.height/2, 400, 50), "Log In", buttS)) {
						logIn ();
						if(parseLogin) {
							PlayerPrefs.SetString("UserName", name);
							PlayerPrefs.SetString("Password", password);
						}
					}
				}
			} else {
				GUI.color = Color.white;
				string greeting = "Hello, " + PlayerPrefs.GetString("UserName") + "!";
				GUI.Label (new Rect(Screen.width/2 - 200, Screen.height/2 - 150, 400, 30), greeting,
				           optLabel);
				if(GUI.Button (new Rect(Screen.width/2 - 200, Screen.height/2 - 100, 400, 50), 
				               "PLAY KAT", buttS) && server.clients.Count > 0) {
					startGame();
				}
			}
			
		}
		
		
		//big box
		/*GUI.color = Color.white;
		GUI.Box (new Rect (Screen.width / 2 - 7 * (keySize + keyPad) - 10 * keyPad - legendWidth / 2 - 5,
		                   Screen.height - 140 - 5, 
		                   (14 * (keySize + keyPad) + 28*keyPad)/2,
		                   138), "", colBox);

		GUI.color = Color.white;
		GUI.Box (new Rect (Screen.width/2 + legendWidth/2 - keyPad,
		                   Screen.height - 140 - 5, 
		                   (14 * (keySize + keyPad) + 28*keyPad)/2,
		                   138), "", colBox);*/

		//Boxes
		GUI.color = leftKB;
		GUI.Box (new Rect(Screen.width/2 - 7*keySize - 20*keyPad - legendWidth/2, 
		                  Screen.height - keySize*4 + 7*keyPad, 
		                  keySize*2 + 5*keyPad, 
		                  keySize*3 + 7*keyPad), "", colBox);

		GUI.Box (new Rect(Screen.width/2 - 5*keySize - 13*keyPad- legendWidth/2, 
		                  Screen.height - keySize*4 + 7*keyPad, 
		                  keySize*3 + 6*keyPad, 
		                  keySize*3 + 7*keyPad), "", colBox);

		GUI.Box (new Rect(Screen.width/2 - 2*keySize - legendWidth/2 - 5*keyPad, 
		                  Screen.height - keySize*4 + 7*keyPad, 
		                  keySize*2 + 5*keyPad, 
		                  keySize*3 + 7*keyPad), "", colBox);

		GUI.color = rightKB;
		GUI.Box (new Rect(Screen.width/2 + legendWidth/2, 
		                  Screen.height - keySize*4 + 7*keyPad, 
		                  keySize*2 + 5*keyPad,
		                  keySize*3 + 7*keyPad), "", colBox);
				
		GUI.Box (new Rect(Screen.width/2 + legendWidth/2 + 2*keySize + 7*keyPad, 
		                  Screen.height - keySize*4 + 7*keyPad,
		                  keySize*3 + 6*keyPad , 
		                  keySize*3 + 7*keyPad), "", colBox);

		GUI.Box (new Rect(Screen.width/2 + legendWidth/2 + 5*keySize + 15*keyPad, 
		                  Screen.height - keySize*4 + 7*keyPad, 
		                  keySize*2 + 5*keyPad,
		                  keySize*3 + 7*keyPad), "", colBox);


		GUI.color = defaultKB;
		//left gestures
		GUI.Label(new Rect(Screen.width/2 - 7*keySize - 18*keyPad - legendWidth/2, 
		                   Screen.height - keySize*4 + 9*keyPad, 
		                   keySize*2 + keyPad, 
		                   keySize - 2*keyPad), "", gestureL);
		
		GUI.Label(new Rect(Screen.width/2 - 5*keySize - 11*keyPad - legendWidth/2, 
		                   Screen.height - keySize*4 + 9*keyPad, 
		                   keySize*3 + 2*keyPad, 
		                   keySize-2*keyPad), "", gestureM);
		
		GUI.Label(new Rect(Screen.width/2 - 2*keySize - 3*keyPad - legendWidth/2, 
		                   Screen.height - keySize*4 + 9*keyPad, 
		                   keySize*2 + keyPad, 
		                   keySize-2*keyPad), "", gestureR);
		
		//right gestures
		GUI.Label(new Rect(Screen.width/2 + legendWidth/2 + 2*keyPad, 
		                   Screen.height - keySize*4 + 9*keyPad,
		                   keySize*2 + keyPad, 
		                   keySize - 2*keyPad), "", gestureL);
		
		GUI.Label(new Rect(Screen.width/2 + legendWidth/2 + 2*keySize + 9*keyPad, 
		                   Screen.height - keySize*4 + 9*keyPad, 
		                   keySize*3 + 2*keyPad, 
		                   keySize-2*keyPad), "", gestureM);
		
		GUI.Label(new Rect(Screen.width/2 + legendWidth/2 + 5*keySize + 17*keyPad, 
		                   Screen.height - keySize*4 + 9*keyPad,  
		                   keySize*2 + keyPad, 
		                   keySize-2*keyPad), "", gestureR);
		
		//Special Keys
		if (legendToggle) {
			GUI.Box (new Rect (Screen.width / 2 - legendWidth / 2.0f, 
		                   Screen.height - keySize * 4 + 7 * keyPad, 
		                   legendWidth,
		                   keySize * 3 + 7 * keyPad), "", vertGestures);
		}


		//R1
		for(int i = 0; i < r1.Length; i++) {
			
			GUI.color = defaultKB;
			
			if(sixFingers && (!l3 || !r3)) {
				GUI.color = deadKB;
			} else if(activeFingers[0][i] == 1) {
				GUI.color = highlight;
			} else if(activeFingers[0][i] == 2) {
				GUI.color = semiHighlight;
			} 
			
			if(i < 2) {
				GUI.Box (new Rect(Screen.width/2 + legendWidth/2 + 2*keyPad + i*(keySize + keyPad), 
				                  Screen.height - keySize*3 + 9*keyPad, 
				                  keySize, keySize + keyPad), keys[0].Substring(i,1), KB); 
			} else if(i < 5) {
				GUI.Box (new Rect(Screen.width/2 + legendWidth/2 + 2*keySize + 9*keyPad + (i-2)*(keySize + keyPad), 
				                  Screen.height - keySize*3 + 9*keyPad, 
				                  keySize, keySize + keyPad), keys[0].Substring(i,1), KB); 
			} else {
				GUI.Box (new Rect(Screen.width/2 + legendWidth/2 + 5*keySize + 17*keyPad + (i-5)*(keySize + keyPad), 
				                  Screen.height - keySize*3 + 9*keyPad, 
				                  keySize, keySize + keyPad), keys[0].Substring(i,1), KB); 
			}
			
		}
		
		//R2
		for(int i = 0; i < r2.Length; i++) {
			
			GUI.color = defaultKB;
			
			if(sixFingers && (!l3 || !r3)) {
				GUI.color = deadKB;
			} else if(activeFingers[1][i] == 1) {
				GUI.color = highlight;
			} else if(activeFingers[1][i] == 2) {
				GUI.color = semiHighlight;
			} 
			
			if(i < 2) {
				GUI.Box (new Rect(Screen.width/2 + legendWidth/2 + 2*keyPad + i*(keySize + keyPad), 
				                  Screen.height - keySize*2 + 11*keyPad, 
				                  keySize, 
				                  keySize + keyPad), keys[1].Substring(i,1), KB); 
			} else if(i < 5) {
				GUI.Box (new Rect(Screen.width/2 + legendWidth/2 + 2*keySize + 9*keyPad + (i-2)*(keySize + keyPad), 
				                  Screen.height - keySize*2 + 11*keyPad, 
				                  keySize, 
				                  keySize + keyPad), keys[1].Substring(i,1), KB); 
			} else {
				GUI.Box (new Rect(Screen.width/2 + legendWidth/2 + 5*keySize + 17*keyPad + (i-5)*(keySize + keyPad), 
				                  Screen.height - keySize*2 + 11*keyPad, 
				                  keySize, 
				                  keySize + keyPad), keys[1].Substring(i,1), KB); 
			}
		}
		
		//L1
		for(int i = 0; i < l1.Length; i++) {
			
			GUI.color = defaultKB;
			
			if(sixFingers && (!l3 || !r3)) {
				GUI.color = deadKB;
			} else if(activeFingers[2][i] == 1) {
				GUI.color = highlight;
			} else if(activeFingers[2][i] == 2) {
				GUI.color = semiHighlight;
			} 

			if(i < 2) {
				GUI.Box (new Rect(Screen.width/2 - 7*keySize - 18*keyPad - legendWidth/2 + i*(keySize + keyPad), 
				                  Screen.height - keySize*3 + 9*keyPad, 
				                  keySize, keySize + keyPad), keys[2].Substring(i,1), KB); 
			} else if(i < 5) {
				GUI.Box (new Rect(Screen.width/2 - 5*keySize - 11*keyPad - legendWidth/2 + (i-2)*(keySize + keyPad), 
				                  Screen.height - keySize*3 + 9*keyPad, 
				                  keySize, keySize + keyPad), keys[2].Substring(i,1), KB); 
			} else {
				GUI.Box (new Rect(Screen.width/2 - 2*keySize - 3*keyPad - legendWidth/2 + (i-5)*(keySize + keyPad), 
				                  Screen.height - keySize*3 + 9*keyPad, 
				                  keySize, keySize + keyPad), keys[2].Substring(i,1), KB); 
			}
		}
		
		//L2
		for(int i = 0; i < l2.Length; i++) {
			
			GUI.color = defaultKB;
			
			if(sixFingers && (!l3 || !r3)) {
				GUI.color = deadKB;
			} else if(activeFingers[3][i] == 1) {
				GUI.color = highlight;
			} else if(activeFingers[3][i] == 2) {
				GUI.color = semiHighlight;
			} 

			if(i < 2) {
				GUI.Box (new Rect(Screen.width/2 - 7*keySize - 18*keyPad - legendWidth/2 + i*(keySize + keyPad), 
				                  Screen.height - keySize*2 + 11*keyPad, 
				                  keySize, keySize + keyPad), 
				         	  	  keys[3].Substring(i, 1), KB); 
			} else if(i < 5) {
				GUI.Box (new Rect(Screen.width/2 - 5*keySize - 11*keyPad - legendWidth/2 + (i-2)*(keySize + keyPad), 
				                  Screen.height - keySize*2 + 11*keyPad, 
				                  keySize, keySize + keyPad), 
				         			keys[3].Substring(i,1), KB); 
			} else {
				GUI.Box (new Rect(Screen.width/2 - 2*keySize - 3*keyPad - legendWidth/2 + (i-5)*(keySize + keyPad), 
				                  Screen.height - keySize*2 + 11*keyPad, 
				                  keySize, keySize + keyPad), 
				         			keys[3].Substring(i,1), KB); 
			}
		}

		//Toggle Trail
		if (Event.current.type == EventType.KeyUp ) {
			if(Event.current.keyCode == KeyCode.T) {
				trailToggle = !trailToggle;
			}
		}
		
		
	}
}
