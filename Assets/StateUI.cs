﻿using UnityEngine;
using System.Collections;

public class StateUI : MonoBehaviour {
	
	GlobalScript globalObj;
	GUIText stateText;
	
	// Use this for initialization
	void Start () {
		
		GameObject g = GameObject.Find ("GlobalObject");
		globalObj = g.GetComponent<GlobalScript> ();
		stateText = gameObject.GetComponent<GUIText> ();
	}
	
	// Update is called once per frame
	void Update () {
		stateText.text = globalObj.state.ToString ();
	}
}
