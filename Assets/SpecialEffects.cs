﻿using UnityEngine;

public class SpecialEffects : MonoBehaviour
{
	private static SpecialEffects instance;
	
	// Prefabs
	public ParticleSystem explosionEffect;
	public GameObject fTrailPrefab;
	public GameObject bTrailPrefab;
	
	void Awake()
	{
		instance = this;
	}
	
	void Start()
	{
		// Check prefabs
		if (explosionEffect == null)
			Debug.LogError("Missing Explosion Effect!");
		if (fTrailPrefab == null)
			Debug.LogError("Missing Forward Trail Prefab!");
		if (bTrailPrefab == null)
			Debug.LogError("Missing Backward Trail Prefab!");			
	}
	

	public static ParticleSystem MakeExplosion(Vector3 position)
	{
		if (instance == null)
		{
			Debug.LogError("There is no SpecialEffectsScript in the scene!");
			return null;
		}
		
		ParticleSystem effect = Instantiate(instance.explosionEffect) as ParticleSystem;
		effect.transform.position = position;
		
		// Program destruction at the end of the effect
		Destroy(effect.gameObject, effect.duration);
		
		return effect;
	}

	public static GameObject MakefTrail(Vector3 position)
	{
		GameObject trail = Instantiate(instance.fTrailPrefab) as GameObject;
		trail.transform.position = position;	
		return trail;
	}

	public static GameObject MakebTrail(Vector3 position)
	{
		GameObject trail = Instantiate(instance.bTrailPrefab) as GameObject;
		trail.transform.position = position;
		
		return trail;
	}
}