﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ServerOutput : MonoBehaviour {

	GUIText outputText;
	public string message;

	public GUIStyle KB;
	public GUIStyle swipesLabel;

	Color deadKB;
	Color defaultKB;
	Color semiHighlight;
	Color highlight;
	
	int keyCount;	//7 keys
	int[][] activeFingers;
	string[] keys;

	string l1;
	string l2;
	string r1;
	string r2;

	bool l3,r3;

	bool sixFingers;

	string mode;
	ServerControl server;

	public Dictionary<int, GameObject> trails;

	void Awake() {
		Application.targetFrameRate = 20;
	}
	
	// Use this for initialization
	void Start () {
		message = "";
		outputText = gameObject.GetComponent<GUIText> ();

		deadKB = Color.Lerp (Color.black, Color.gray, 0.5f);
		defaultKB = Color.Lerp (Color.white, Color.gray, 0.6f);
		semiHighlight = Color.Lerp (Color.red, Color.yellow, 0.7f);
		highlight = Color.yellow;
		keyCount = 7;

		mode = "alpha";

		l1 = "lsue,cq";
		l2 = "digrxkv";
		r1 = "wb.aotm";
		r2 = "jyznfhp";

		trails = new Dictionary<int, GameObject> ();	
		keys = new string[4] {r1, r2, l1, l2};
		activeFingers = new int[4][] {new int[keyCount], new int[keyCount], 
			new int[keyCount], new int[keyCount]};

		GameObject g = GameObject.Find ("Sockets");
		server = g.GetComponent<ServerControl> ();

	}
	
	// Update is called once per frame
	void Update () {

		if(outputText.text.Length % 45 == 0) {
			outputText.text += '\n';
		}

		int currentLength = message.Length;
		while(currentLength > 0) {
			if(outputText.text.Length % 45 == 0) {
				outputText.text += '\n';
			}
			switch(message[0]) {
				case ((char) 21):
					getMappedKey(message.Substring(1, 3));
					if(currentLength >= 4) {
						currentLength -= 4;
						message = message.Substring(4, currentLength);
					}
				break;	

				case ((char) 22): 		
					updateActive(message.Substring(1, 3));
					if(currentLength >= 4) {
						currentLength -= 4;
						message = message.Substring(4, currentLength);
					}
				break;

				case ((char) 23):
					refreshKeyBoard();
					if(currentLength >= 1) {
						currentLength--;
						message = message.Substring(1, currentLength);
					}
				break;

				case ((char) 24):
					if(currentLength >= 1) {
						currentLength--;
						message = message.Substring(1, currentLength);
					} 
					reset ();
				break;

				case ((char) 25):
					int trailID = int.Parse(message.Substring(1,1));
					currentLength -= 2;

					if(message[2] == (char) 27) {
						if (trails.ContainsKey(trailID)) {
							GameObject trail = trails[trailID];
							Destroy (trail, trail.GetComponent<TrailRenderer>().time);
							trails.Remove(trailID);
						}
						currentLength--;
						message = message.Substring (3, currentLength);
					} else {
						message = message.Substring(2, currentLength);
						string x = "";
						string y = "";

						while(message[0] != (char) 26) {
							x += message[0];
							currentLength--;
							message = message.Substring(1, currentLength);
						}
						currentLength--;
						message = message.Substring(1, currentLength);
						
						while(message[0] != (char) 26) {
							y += message[0];
							currentLength--;
							message = message.Substring(1, currentLength);
						}
						currentLength--;
						message = message.Substring(1, currentLength);
						
						Vector3 oldPos = new Vector3(float.Parse(x), float.Parse(y), 0.0f);
						Vector3 temp = Camera.main.WorldToScreenPoint(oldPos);
						Vector3 position = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width - temp.x, temp.y, temp.z));

						if(!trails.ContainsKey(trailID)) {
							//make trail
							GameObject trail = SpecialEffects.MakefTrail(position);
							trails.Add (trailID, trail);
						} else {
							//move trail (if difference is too big might be another finger)
							GameObject trail = trails[trailID];
							if(Mathf.Abs(trail.transform.position.x - position.x) <= 10.0f
						   		&& Mathf.Abs (trail.transform.position.y - position.y) <= 10.0f) {
								trail.transform.position = position;
							} else {
								trails.Remove(trailID);
								//GameObject newTrail = SpecialEffects.MakefTrail(position);
								//trails.Add (trailID, newTrail);
							}
						}
					}
				break;
				case (char)28:
					server.clients.RemoveAt(server.clients.Count - 1);
					currentLength--;
					message = message.Substring(1, currentLength);
					reset();
				break;
				case (char)29:
					if(message.Substring(1, 1) == "0") {
						sixFingers = false;
					} else if(message.Substring(1, 1) == "1") {
						sixFingers = true;
					}
					currentLength = currentLength - 2;
					message = message.Substring(2, currentLength);
				break;
				case (char)30:
					if(message.Substring(1, 1) == "0") {
						l3 = false;
					} else if(message.Substring(1, 1) == "1") {
						l3 = true;
					}
					currentLength = currentLength - 2;
					message = message.Substring(2, currentLength);
				break;
				case (char)31:
					if(message.Substring(1, 1) == "0") {
						r3 = false;
					} else if(message.Substring(1, 1) == "1") {
						r3 = true;
					}
					currentLength = currentLength - 2;
					message = message.Substring(2, currentLength);
				break;
			}
		}		
	}

	void updateActive(string s) {

		//receive message here should include currentFinger, gestures
		//if s has length 2 then one transition, if 3 then 2 transitions
		
		int currentFinger = int.Parse(s.Substring (0, 1));

		
		if(currentFinger < 4) {
			if(s.Substring(2,1) == "_") {
				switch(s.Substring (1,1)) {
				case "|":
					activeFingers[currentFinger][4] = 2;
					activeFingers[currentFinger][3] = 1;
					activeFingers[currentFinger][2] = 2;
					break;
				case ">":
					activeFingers[currentFinger][1] = 1;
					activeFingers[currentFinger][0] = 2;
					break;
				case "<":
					activeFingers[currentFinger][6] = 2;
					activeFingers[currentFinger][5] = 1;
					break;
				case "^":
					break;
				case "v":
					break;
				}
			} else if(s.Length == 3) {
				switch (s.Substring (1,1)) {
				case "|":
					switch (s.Substring (2,1)) {
					case "-":
						activeFingers[currentFinger][3] = 1;
						break;
					case ">":
						activeFingers[currentFinger][2] = 1;
						break;
					case "<": 
						activeFingers[currentFinger][4] = 1;
						break;
					case "^":
						break;
					case "v":
						break;
					}
					break;
				case ">":
					switch (s.Substring (2,1)) {
					case "-":
						activeFingers[currentFinger][1] = 1;
						break;
					case "|": 
						activeFingers[currentFinger][0] = 1;
						break;
					} 
					break;
				case "<":
					switch (s.Substring (2,1)) {
					case "-":
						activeFingers[currentFinger][5] = 1;
						break;
					case "|": 
						activeFingers[currentFinger][6] = 1;
						break;
					} 
					break;
				case "^":
					break;
				case "v":
					break;
				}
			}
		}
	}
	
	void getMappedKey(string s) {
		
		//Printing Mapped Characters
		int currentFinger = int.Parse(s.Substring (0, 1));

		if(currentFinger < 4) {
			switch (s.Substring (1, 1)) {
			case "|":
				switch (s.Substring (2, 1)) {
				case "-":
					outputText.text += keys[currentFinger].Substring(3, 1);
					break;
				case ">":
					outputText.text += keys[currentFinger].Substring(2, 1);
					break;
				case "<": 
					outputText.text += keys[currentFinger].Substring(4, 1);
					break;
				}
				modeUpdate();
				break;
			case ">":
				switch (s.Substring (2, 1)) {
				case "-":
					outputText.text += keys[currentFinger].Substring(1, 1);
					break;
				case "|": 
					outputText.text += keys[currentFinger].Substring(0, 1);
					break;
				} 
				modeUpdate();
				break;
			case "<":
				switch (s.Substring (2, 1)) {
				case "-":
					outputText.text += keys[currentFinger].Substring(5, 1);
					break;
				case "|": 
					outputText.text += keys[currentFinger].Substring(6, 1);
					break;
				} 
				modeUpdate();
				break;
			case "^":
				switch (s.Substring (2, 1)) {
				case "-": 
					//right hand Up Swipe
					if(currentFinger == 0 || currentFinger ==1) {
						outputText.text = outputText.text.Substring(0, outputText.text.Length-1);
					} else {
						if(mode != "numLock") {
							mode = "numLock";
							modeUpdate();
						} else {
							mode = "alpha";
							modeUpdate();
						}
					}
					modeUpdate();
					break;
				case "|":
					//right hand Up Hold
					if(currentFinger == 0 || currentFinger ==1) {
						reset();
					} else {
						//left hand Up Hold
						if(mode != "specialLock") {
							mode = "specialLock";
							modeUpdate();
						} else {
							mode = "alpha";
							modeUpdate();
						}
					}
					break;
				}
				break;
			case "v":
				switch (s.Substring (2, 1)) {
				case "-": 
					//right hand Down Swipe
					if(currentFinger == 0 || currentFinger ==1) {
						outputText.text += " ";
					} else {
						//left hand Down Swipe
						if(mode != "CAPSLock") {
							keys[2] = capitalize(l1);
							keys[3] = capitalize(l2);
							keys[0] = capitalize(r1);
							keys[1] = capitalize(r2);
						} else {
							keys[2] = l1;
							keys[3] = l2;
							keys[0] = r1;
							keys[1] = r2;
						}

					}
					break;
				case "|":
					//right hand Down Hold
					if(currentFinger == 0 || currentFinger ==1) {
						outputText.text += char.ToString('\n');
					} else {
						//left hand Down Hold
						if(mode != "CAPSLock") {
							mode = "CAPSLock";
							modeUpdate();
						} else {
							mode = "alpha";
							modeUpdate();
						}
					}
					break;
				}
				break;
			}
		} else if(currentFinger < 6) {
			switch (s.Substring (1, 1)) {
			case "<":
				switch (s.Substring (2, 1)) {
				case "-": 
					//right hand Up Swipe
					if(currentFinger == 4) {
						outputText.text += " ";
					}
					modeUpdate();
					break;
				case "|":
					//right hand Up Hold
					if(currentFinger == 4) {
						if(mode != "numLock") {
							mode = "numLock";
							modeUpdate();
						} else {
							mode = "alpha";
							modeUpdate();
						}
					} else {
						//left hand Up Hold
						if(mode != "specialLock") {
							mode = "specialLock";
							modeUpdate();
						} else {
							mode = "alpha";
							modeUpdate();
						}
					}
					break;
				}
				break;
			case ">":
				switch (s.Substring (2, 1)) {
				case "-": 
					//right hand Down Swipe
					if(currentFinger == 4) {
						outputText.text = outputText.text.Substring(0, outputText.text.Length-1);
					} else {
						//left hand Down Swipe
						if(mode != "CAPSLock") {
							keys[2] = capitalize(l1);
							keys[3] = capitalize(l2);
							keys[0] = capitalize(r1);
							keys[1] = capitalize(r2);
						} else {
							keys[2] = l1;
							keys[3] = l2;
							keys[0] = r1;
							keys[1] = r2;
						}
					
					}
					break;
				case "|":
					//right hand Down Hold
					if(currentFinger == 4) {
						outputText.text += char.ToString('\n');
					} else {
						//left hand Down Hold
						if(mode != "CAPSLock") {
							mode = "CAPSLock";
							modeUpdate();
						} else {
							mode = "alpha";
							modeUpdate();
						}
					}
					break;
				}
				break;
			}
		}
	}
	
	void refreshKeyBoard() {
		activeFingers = new int[4][] {new int[keyCount], new int[keyCount], 
			new int[keyCount], new int[keyCount]};
	}

	void reset() {
		keyCount = 7;
		outputText.text = "";
		

		refreshKeyBoard();
		mode = "alpha";
		modeUpdate();
	}

	void modeUpdate() {

		string l1New = l1;
		string l2New = l2;
		string r1New = r1;
		string r2New = r2;

		switch(mode) {
		case "alpha":
			l1New = l1;
			l2New = l2;
			r1New = r1;
			r2New = r2;
			break;
		case "CAPSLock":
			l1New = capitalize(l1);
			l2New = capitalize(l2);
			r1New = capitalize(r1);
			r2New = capitalize(r2);
			break;
		case "numLock":
			l1New = "12345+-";
			l2New = "67890*/";
			r1New = "=|%()[]";
			r2New = "!#<>^:;";
			break;
		case "specialLock":
			l1New = ".,?!@#$";
			r1New = "~*|()-_";
			r2New = "+=%[]^&";
			l2New = "\"'`{}\\/";
			break;
		}

		keys [0] = r1New;
		keys [1] = r2New;
		keys [2] = l1New;
		keys [3] = l2New;
	}

	string capitalize(string s) {
		
		if(s.Length >= 1) {
			char[] orig = s.ToCharArray();
			for(int i = 0; i < orig.Length; i++) {
				if(char.IsLower(orig[i])) {
					orig[i] = char.ToUpper(orig[i]);
				}
			}
			return new string(orig);
		} else {
			//return illegal argument
			return s;
		}
	}

	void OnGUI() {
		//R1
		for(int i = 0; i < r1.Length; i++) {
					
			GUI.color = defaultKB;
			
			if(sixFingers && (!l3 || !r3)) {
				GUI.color = deadKB;
			} else if(activeFingers[0][i] == 1) {
				GUI.color = highlight;
			} else if(activeFingers[0][i] == 2) {
				GUI.color = semiHighlight;
			} 
			
			GUI.Box (new Rect(Screen.width - 300 + i*42, Screen.height-120, 35, 40), keys[0].Substring(i, 1), KB);
			
		}
		
		//R2
		for(int i = 0; i < r2.Length; i++) {
			
			GUI.color = defaultKB;
			
			if(sixFingers && (!l3 || !r3)) {
				GUI.color = deadKB;
			} else if(activeFingers[1][i] == 1) {
				GUI.color = highlight;
			} else if(activeFingers[1][i] == 2) {
				GUI.color = semiHighlight;
			} 
			
			GUI.Box (new Rect(Screen.width - 300 + i*42, Screen.height-70, 35, 40), keys[1].Substring(i, 1), KB); 
		}
		
		//L1
		for(int i = 0; i < l1.Length; i++) {
			
			GUI.color = defaultKB;
			
			if(sixFingers && (!l3 || !r3)) {
				GUI.color = deadKB;
			} else if(activeFingers[2][i] == 1) {
				GUI.color = highlight;
			} else if(activeFingers[2][i] == 2) {
				GUI.color = semiHighlight;
			} 
			
			GUI.Box (new Rect(20 + i*42, Screen.height-120, 35, 40), keys[2].Substring(i,1), KB); 
		}
		
		//L2
		for(int i = 0; i < l2.Length; i++) {
			
			GUI.color = defaultKB;
			
			if(sixFingers && (!l3 || !r3)) {
				GUI.color = deadKB;
			} else if(activeFingers[3][i] == 1) {
				GUI.color = highlight;
			} else if(activeFingers[3][i] == 2) {
				GUI.color = semiHighlight;
			} 
			GUI.Box (new Rect(20 + i*42, Screen.height-70, 35, 40), keys[3].Substring(i, 1), KB); 
		}

		if(!sixFingers) {
			//left 
			//up and hold
			GUI.color = defaultKB;
			GUI.Label (new Rect(Screen.width/2 - 120, Screen.height - 97, 70, 20),  
			           "__", swipesLabel) ;
			GUI.Label (new Rect(Screen.width/2 - 120, Screen.height - 80, 70, 20),  
			           '\u2191' + " SpecialChars", swipesLabel) ;
			//down
			GUI.Label (new Rect(Screen.width/2 - 120, Screen.height - 30, 70, 20),  
			           '\u2193' + " CAPS", swipesLabel) ;
			//down and hold 
			GUI.Label (new Rect(Screen.width/2 - 120, Screen.height - 60, 70, 20),  
			           '\u2193' + " CAPSLock", swipesLabel) ;
			GUI.Label (new Rect(Screen.width/2 - 120, Screen.height - 57, 70, 20),  
			           "__", swipesLabel);
			
			//right 
			//up and hold
			GUI.Label (new Rect(Screen.width/2 + 10, Screen.height - 97, 70, 20),  
			           "__", swipesLabel) ;
			GUI.Label (new Rect(Screen.width/2 + 10, Screen.height - 80, 70, 20),  
			           '\u2191' + " Numbers", swipesLabel) ;
			// up
			GUI.Label (new Rect(Screen.width/2 + 10, Screen.height - 110, 70, 20),  
			           '\u2191' + " BackSpace", swipesLabel) ;
			// down
			GUI.Label (new Rect(Screen.width/2 + 10, Screen.height - 30, 70, 20),  
			           '\u2193' + " Space", swipesLabel) ;
			// down and hold
			GUI.Label (new Rect(Screen.width/2 + 10, Screen.height - 60, 70, 20),  
			           '\u2193' + " Return", swipesLabel);
			GUI.Label (new Rect(Screen.width/2 + 10, Screen.height - 57, 70, 20),  
			           "__", swipesLabel);
		} else {
			//left hand
			//left and hold
			GUI.color = defaultKB;
			GUI.Label (new Rect(Screen.width/2 - 120, Screen.height - 80, 70, 20),  
			           '\u2192' + "|" + " SpecialChars", swipesLabel) ;
			//right
			GUI.Label (new Rect(Screen.width/2 - 120, Screen.height - 30, 70, 20),  
			           '\u2190' + " CAPS", swipesLabel) ;
			//right and hold 
			GUI.Label (new Rect(Screen.width/2 - 120, Screen.height - 60, 70, 20),  
			           "|" + '\u2190' +" CAPSLock", swipesLabel) ;
			
			//right hand
			//left and hold
			GUI.Label (new Rect(Screen.width/2 + 10, Screen.height - 80, 70, 20),  
			           '\u2192' + "|"+ " Numbers", swipesLabel) ;
			//left
			GUI.Label (new Rect(Screen.width/2 + 10, Screen.height - 110, 70, 20),  
			           '\u2192' + " Space", swipesLabel) ;
			// right
			GUI.Label (new Rect(Screen.width/2 + 10, Screen.height - 30, 70, 20),  
			           '\u2190' + " BackSpace", swipesLabel) ;
			// right and hold
			GUI.Label (new Rect(Screen.width/2 + 10, Screen.height - 60, 70, 20),  
			           "|" + '\u2190'  + " Return", swipesLabel);
		}
				
	}
}
