﻿using UnityEngine;
using System.Collections;
using System.Net;
using System.Net.Sockets;
using System.Text;

public class ClientControl : MonoBehaviour {

	const int kInputWidth = 100;

	public string input = "";
	public bool send = false;
	Socket socket;

	void onClientStarted(Socket socket)
	{
		Debug.Log ("client started");

		this.socket = socket;
		SocketRead.Begin (socket, OnReceive, OnError);
	}
	
	
	void OnReceive (SocketRead read, byte[] data)
	{
		Debug.Log (Encoding.ASCII.GetString (data, 0, data.Length));
	}
	

	void OnError (SocketRead read, System.Exception exception)
	{
		Debug.LogError ("Receive error: " + exception);
	}
	
	
	void Update() {
		if (socket == null)
		{
			return;
		}
		
		if (send)
		{
			send = false;		
			input = input.Trim ();
			input = input.Substring (0, input.Length < SocketRead.kBufferSize ? input.Length : SocketRead.kBufferSize);
			socket.Send (Encoding.ASCII.GetBytes (input));
			input = "";
		}
	}
}
